export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const RESRT_THE_CART = "RESRT_THE_CART";

export const addToCart = (product, QUT, whatToPutInsideAr) => {
  return {
    type: ADD_TO_CART,
    product: product,
    QUT: QUT,
    whatToPutInsideAr: whatToPutInsideAr,
  };
};

export const removeFromCart = (productId, whatToPutInsideAr) => {
  return { type: REMOVE_FROM_CART, pid: productId , whatToPutInsideAr: whatToPutInsideAr};
};

export const resetTheCart = () =>{
  return {type: RESRT_THE_CART};
};
