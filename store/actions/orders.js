export const ADD_ORDER = "ADD_ORDER";
export const RESET_ORDERS = "RESET_ORDERS";

export const addOrder = (cartItem, totalAmount, whatToPutInsideAr) => {
  return {
    type: ADD_ORDER,
    orderData: {
      items: cartItem,
      amount: totalAmount,
      whatToPutInsideAr: whatToPutInsideAr,
    },
  };
};

export const resetOrders = () => {
  return{
    type: RESET_ORDERS
  }
}
