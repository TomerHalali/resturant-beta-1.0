export const SET_MEALS = "SET_MEALS";
export const SET_CATEGORIES = "SET_CATEGORIES";
export const SET_EXPRESS_PRODUCTS = "SET_EXPRESS_PRODUCTS";

export const getAllMenuMeals = (meals) => {
  return {
    type: SET_MEALS,
    meals: meals,
  };
};

export const getAllCategories = (categories) => {
  return {
    type: SET_CATEGORIES,
    categories: categories,
  };
};

export const getAllExpressProducts = (expressProducts) => {

  return {
    type: SET_EXPRESS_PRODUCTS,
    expressProducts: expressProducts,
    
  };
};
