import {
  SET_MEALS,
  SET_CATEGORIES,
  SET_EXPRESS_PRODUCTS,
} from "../actions/meals";

const initialState = {
  meals: {},
  categories: {},
  expressProducts: {}
};

const mealsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CATEGORIES:
      return {
        ...state,
        categories: { ...state.categories, ["All"]: action.categories },
      };
    case SET_MEALS:
      return {
        ...state,
        meals: { ...state.meals, ["All"]: action.meals },
      };
    case SET_EXPRESS_PRODUCTS:
      return {
        ...state,
        expressProducts: { ...state.expressProducts, ["All"]: action.expressProducts }
      }

    default:
      return state;
  }
};

export default mealsReducer;
