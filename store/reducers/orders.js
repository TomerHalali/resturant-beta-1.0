import Order from "../../models/order";
import { ADD_ORDER, RESET_ORDERS } from "../actions/orders";

const initialState = {
  orders: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_ORDER:
      const whatInside = action.whatToPutInsideAr;

      if(state.orders.length === 0){
        const newOrder = new Order(
          new Date().toString(),
          action.orderData.items,
          action.orderData.amount,
          new Date(),
          whatInside

        );
        return {
          ...state,
          orders: state.orders.concat(newOrder),
          
        };
      }else{
        const resumeOrder = new Order(
          new Date().toString(),
          action.orderData.items,
          action.orderData.amount,
          new Date(),
          whatInside
        );
        return {
          ...state,
          orders: state.orders.pop(),
          orders: state.orders.concat(resumeOrder),
        }
      }

      case RESET_ORDERS:

      return {
        orders: []
      }
      
  }

  return state;
};
