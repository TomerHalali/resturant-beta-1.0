import { ADD_TO_CART, REMOVE_FROM_CART, RESRT_THE_CART } from "../actions/cart";
import CartItem from "../../models/cart-item";

const initialState = {
  items: {},
  totalAmount: 0,
  totalItems: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      const addedProduct = action.product;
      const prodPrice = addedProduct.price;
      const prodTitle = addedProduct.title;
      const whatInside = action.whatToPutInsideAr;

      let showToCart = "";
      for (const property in whatInside) {
        if (whatInside[property]) {
          showToCart = showToCart.concat(`${property}`, " ");
        }
      }

      let newItem;
      newItem = new CartItem(
        action.QUT,
        prodPrice,
        prodTitle,
        prodPrice * action.QUT,
        showToCart
      );

      return {
        ...state,
        items: { ...state.items, [Math.random()]: newItem },
        totalAmount: state.totalAmount + prodPrice * action.QUT,
        totalItems: state.totalItems + action.QUT
      };

    case REMOVE_FROM_CART:
      const selectedCartItem = state.items[action.pid];
      const currentQty = selectedCartItem.quantity;
      const whatInsideRemove = action.whatToPutInsideAr;

      if (currentQty > 1) {
        // need to reduce it, not erase it
        const updatedCartItem = new CartItem(
          selectedCartItem.quantity - 1,
          selectedCartItem.productPrice,
          selectedCartItem.productTitle,
          selectedCartItem.sum - selectedCartItem.productPrice,
          whatInsideRemove
        );
        updatedCartItems = { ...state.items, [action.pid]: updatedCartItem };
      } else {
        updatedCartItems = { ...state.items };
        delete updatedCartItems[action.pid];
      }
      return {
        ...state,
        items: updatedCartItems,
        totalAmount: state.totalAmount - selectedCartItem.productPrice,
        totalItems: state.totalItems - 1
      };

    case RESRT_THE_CART:
      return {
        items: {},
        totalAmount: 0,
        totalItems: 0
      };
  }

  return state;
};
