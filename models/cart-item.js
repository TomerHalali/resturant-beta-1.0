class CartItem {
    constructor (quantity, productPrice, productTitle, sum, whatInside) {
        this.quantity = quantity;
        this.productPrice = productPrice;
        this.productTitle = productTitle;
        this.sum = sum;
        this.whatInside = whatInside
    }
}

export default CartItem;