export const sendValidationCodeToThePhone = (phoneNumber) => {
    fetch(`https://stark-ravine-11171.herokuapp.com/sendValidCodeMessage`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          phoneNumber: phoneNumber,
        }),
      });
};
