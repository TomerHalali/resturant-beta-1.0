var nodemailer = require("nodemailer");
const fs = require("fs");
const replaceTemplate = require('./modules/replaceTemplate');


const tempEmail = fs.readFileSync(
  `${__dirname}/html-pages/template_receipt.html`,
  "utf-8"
);
const resUser= {
street: 'הרחוב הגדול',
apaNumber: 2,
entarece: "2",
floor: 4,
apartment: 5,
nId: 454,
phoneNumber: 555555555,
fullName: "תומר הללי",
  theOrder: {
    id: 121212,
    totalAmount: 256,
    items:[]
  }
}
const output = replaceTemplate(tempEmail, resUser);

var transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "EMAILADDRESS",
    pass: "PASSWORDOFMAIL",
  },
});

var mailOptions = {
  from: "TomerHalali@gmail.com",
  to: "TomerHalali@gmail.com",
  subject: "פירוט הזמנה המסעדה באר שבע",
  html: output,
};

transporter.sendMail(mailOptions, function (error, info) {
  if (error) {
    console.log(error);
  } else {
    console.log("Email sent: " + info.response);
  }
});

//https://myaccount.google.com/lesssecureapps?pli=1&rapt=AEjHL4P96xeTRpl-OPG3vx71JQkFR6AEjFSgKFpyBfJq4wJ_KsqD0VxmBatIcNCKGLCKK2XLtTxktQIgGFNH8DGLV1ZbVBRrGw
