module.exports = async function (req, res, next) {
  // GET token from Cookies

  const loginPage = await HtmlPages.find({
    title: "tempLogin", 
  });

  const bearerHeader = req.cookies["token"];

  if (typeof bearerHeader !== "undefined") {
    req.token = bearerHeader;
    next();
  } else {
    res.status(200).send(loginPage[0].htmlCode);
  }
};
