const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config({ path: "../config.env" });
const express = require("express");
const app = express();
const restaurantRouter = require("./routes/restaurantRoutes");
var cookieParser = require('cookie-parser');

//////////////////// Server Listen ////////////////////

app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(cookieParser());

app.use(express.json({ exteended: false }));
app.use("/", restaurantRouter);

const port = 3000;
app.listen(port, () => {
  console.log(`app running on port ${port}...`);
});

//////////////////MongoDB - connect //////////////////

const DB = process.env.DATABASE.replace(
  "<PASSWORD>",
  process.env.DATABASE_PASSWORD
);

mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then((con) => {
    console.log("DB Connection successful!");
  });
