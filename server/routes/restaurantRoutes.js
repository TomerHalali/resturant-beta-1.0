const replaceTemplate = require("../modules/replaceTemplate");
var nodemailer = require("nodemailer");
var md5 = require("md5");
var jwt = require("jsonwebtoken");
const express = require("express");

const restaurantRouter = express.Router();

const Categories = require("../modules/categoriesSchema");
const Meals = require("../modules/mealsSchema");
const Products = require("../modules/productsSchema");
const Reservation = require("../modules/reservationSchema");
const Users = require("../modules/usersSchema");
const Receipt = require("../modules/receiptSchema");
const HtmlPages = require("../modules/htmlPagesSchema");
const Availables = require("../modules/availablesSchema");

const auth = require("../middleware/auth");

const dotenv = require("dotenv");
dotenv.config({ path: "../../config.env" });
const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require("twilio")(accountSid, authToken);
const JWTSecret = process.env.JWT_SECRET;

///////////////////////////////////////////////////////

//////////////////// GET METHODS ////////////////////

const getAvailable = async (req, res) => {
  const allAvailables = await Availables.find();
  res.status(200).json({
    status: "success",
    data: allAvailables,
  });
};

const getAllCategories = async (req, res) => {
  const allCategories = await Categories.find();
  res.status(200).json({
    status: "success",
    numberOfResults: allCategories.length,
    data: {
      allCategories,
    },
  });
};

const getAllMeals = async (req, res) => {
  const allMeals = await Meals.find();
  res.status(200).json({
    status: "success",
    numberOfResults: allMeals.length,
    data: {
      allMeals,
    },
  });
};

const getAllProducts = async (req, res) => {
  const allProducts = await Products.find();
  res.status(200).json({
    status: "success",
    numberOfResults: allProducts.length,
    data: {
      allProducts,
    },
  });
};

const getAllResevations = async (req, res) => {
  const allReservations = await Reservation.find();
  res.status(200).json({
    status: "success",
    numberOfResults: allReservations.length,
    data: {
      allReservations,
    },
  });
};

const getMainPage = async (req, res) => {
  const allReservations = await Reservation.find({
    status: "התקבלה",
  });

  const reservationBlock = await HtmlPages.find({
    title: "tempReservation",
  });

  const sitePage = await HtmlPages.find({
    title: "tempSite",
  });

  jwt.verify(req.token, JWTSecret, (err, authData) => {
    if (err) {
      res.status(403).end("FAILD!");
    } else {
      const cardsHtml = allReservations
        .map((el) => replaceTemplate(reservationBlock[0].htmlCode, el))
        .join("");
      const output = sitePage[0].htmlCode
        .replace("{%PRODUCT_CARDS%}", cardsHtml)
        .replace("{%STATUS%}", "הזמנות חדשות שהתקבלו");

      res.status(200).end(output);
    }
  });
};

const getFinishPage = async (req, res) => {
  const finishReservations = await Reservation.find({
    status: "הסתיים",
  });

  const reservationBlock = await HtmlPages.find({
    title: "tempReservation",
  });

  const sitePage = await HtmlPages.find({
    title: "tempSite",
  });

  jwt.verify(req.token, JWTSecret, (err, authData) => {
    if (err) {
      res.status(403).end("FAILD!");
    } else {
      const cardsHtml = finishReservations
        .map((el) => replaceTemplate(reservationBlock[0].htmlCode, el))
        .join("");
      const output = sitePage[0].htmlCode
        .replace("{%PRODUCT_CARDS%}", cardsHtml)
        .replace("{%STATUS%}", "הזמנות שהסתיימו");
      res.status(200).end(output);
    }
  });
};
const getPreparationPage = async (req, res) => {
  const preparationReservations = await Reservation.find({
    status: "בטיפול",
  });

  const reservationBlock = await HtmlPages.find({
    title: "tempReservation",
  });

  const sitePage = await HtmlPages.find({
    title: "tempSite",
  });

  jwt.verify(req.token, JWTSecret, (err, authData) => {
    if (err) {
      res.status(403).end("FAILD!");
    } else {
      const cardsHtml = preparationReservations
        .map((el) => replaceTemplate(reservationBlock[0].htmlCode, el))
        .join("");
      const output = sitePage[0].htmlCode
        .replace("{%PRODUCT_CARDS%}", cardsHtml)
        .replace("{%STATUS%}", "הזמנות בטיפול");
      res.status(200).end(output);
    }
  });
};

const getReservationInfo = async (req, res) => {
  const reservationInfoPage = await HtmlPages.find({
    title: "tempReservationInfo",
  });

  const allReservations = await Reservation.find();
  const resUser = allReservations[req.params.nId - 1];
  const output = replaceTemplate(reservationInfoPage[0].htmlCode, resUser);
  res.status(200).end(output);
};

//////////////////// POST METHODS ////////////////////
const postToCloseTheDay = async (req, res) => {
  await Reservation.updateMany(
    { status: "התקבלה" },
    { $set: { status: "נסגר" } }
  );
  await Reservation.updateMany(
    { status: "בטיפול" },
    { $set: { status: "נסגר" } }
  );
  await Reservation.updateMany(
    { status: "הסתיים" },
    { $set: { status: "נסגר" } }
  );

  const allReservations = await Reservation.find({
    status: "התקבלה",
  });

  const reservationBlock = await HtmlPages.find({
    title: "tempReservation",
  });

  const sitePage = await HtmlPages.find({
    title: "tempSite",
  });

  const cardsHtml = allReservations
    .map((el) => replaceTemplate(reservationBlock[0].htmlCode, el))
    .join("");
  const output = sitePage[0].htmlCode
    .replace("{%PRODUCT_CARDS%}", cardsHtml)
    .replace("{%STATUS%}", "הזמנות חדשות שהתקבלו");

  res.status(200).end(output);
};

const postOnCurrentPage = async (req, res) => {
  if (req.body.status != null) {
    await Reservation.updateOne(
      { nId: parseInt(req.body.nId) },
      { $set: { status: req.body.status } }
    );
  }

  const allReservations = await Reservation.find({
    status: "התקבלה",
  });

  const reservationBlock = await HtmlPages.find({
    title: "tempReservation",
  });

  const sitePage = await HtmlPages.find({
    title: "tempSite",
  });

  const cardsHtml = allReservations
    .map((el) => replaceTemplate(reservationBlock[0].htmlCode, el))
    .join("");
  const output = sitePage[0].htmlCode
    .replace("{%PRODUCT_CARDS%}", cardsHtml)
    .replace("{%STATUS%}", "הזמנות חדשות שהתקבלו");

  res.status(200).end(output);
};

const postANewReservation = async (req, res) => {
  const newResevation = req.body; //create a new object with exiest object //npm install body-parser | Object.assign(req.body);

  const allReservations = await Reservation.find();
  const id = allReservations.length + 1;

  //mail template
  const emailPage = await HtmlPages.find({
    title: "tempEmailReceipt", //emailPage[0].htmlCode
  });
  const details = await Receipt.find();

  newResevation.idReceipt = id;
  newResevation.logo = details[0].logoReceipt;

  var output = replaceTemplate(emailPage[0].htmlCode, newResevation);

  // Create Document into the module
  const InsertToDBReservation = new Reservation({
    //new object
    nId: id,
    fullName: newResevation.fullName,
    street: newResevation.street,
    apaNumber: newResevation.apaNumber,
    floor: newResevation.floor,
    entarece: newResevation.entarece,
    apartment: newResevation.apartment,
    phoneNumber: newResevation.phoneNumber,
    email: newResevation.email,
    status: newResevation.status,
    theOrder: newResevation.theOrder,
  });

  //save into the collection
  InsertToDBReservation.save()
    .then((doc) => {
      // console.log(doc);
      console.log("ADD TO DATABASE");
      res.status(200).send("ADD TO DATABASE");
    })
    .catch((err) => {
      console.log("ERROR ", err);
    });

  ////post Email To The Client/////
  var transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "EMAIL ADDRESS",
      pass: "PASSWORD OF MAIL",
    },
  });

  var mailOptions = {
    from: "TomerHalali@gmail.com",
    to: newResevation.email,
    subject: "פירוט הזמנה המסעדה באר שבע",
    html: output,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
};

const postSendValidCodeMessage = async (req, res) => {
  client.verify
    .services("VA4aa27e476d1660103f7181c5b2f2ad65")
    .verifications.create({
      to: req.body.phoneNumber,
      channel: "sms",
    })
    .then((verification) => res.status(200).end(verification.status));
};

const postVerifyCodeMessage = async (req, res) => {
  //.end(verification_check.status)
  client.verify
    .services("VA4aa27e476d1660103f7181c5b2f2ad65")
    .verificationChecks.create({
      to: req.body.phoneNumber,
      code: req.body.code,
    })
    .then((verification_check) =>
      res.status(200).json({
        status: "success",
        itsValid: verification_check.status,
      })
    );
};

const postLogin = async (req, res) => {
  const validPage = await HtmlPages.find({
    title: "tempInvalidPage",
  });

  var mdPass = md5(req.body.password);
  mdPass = mdPass.toUpperCase();
  const adminUserId = await Users.find({ userName: "Admin" });

  if (
    req.body.userName == adminUserId[0].userName &&
    mdPass == adminUserId[0].userPassword
  ) {
    const payload = {
      user: {
        id: adminUserId[0]._id,
      },
    };

    const token = jwt.sign(payload, JWTSecret);

    res.cookie("token", token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24,
      sameSite: true,
    });

    res.redirect("/");
  } else {
    res.status(200).send(validPage[0].htmlCode);
  }
};

restaurantRouter.route("/newReservation").post(postANewReservation);
restaurantRouter.route("/sendValidCodeMessage").post(postSendValidCodeMessage);
restaurantRouter.route("/sendVerifyCodeMessage").post(postVerifyCodeMessage);

restaurantRouter.route("/postToCloseTheDay").post(postToCloseTheDay);
restaurantRouter.route("/").get(auth, getMainPage).post(postOnCurrentPage);
restaurantRouter.route("/getFinishReservations").get(auth, getFinishPage);
restaurantRouter
  .route("/getPreparationReservations")
  .get(auth, getPreparationPage);

restaurantRouter.route("/getAvailableExpress").get(getAvailable);

restaurantRouter.route("/getCategories").get(getAllCategories);
restaurantRouter.route("/getMeals").get(getAllMeals);
restaurantRouter.route("/getProducts").get(getAllProducts);
restaurantRouter.route("/getAllResevations").get(getAllResevations);
restaurantRouter.route("/info/:nId").get(getReservationInfo);

restaurantRouter.route("/login").post(postLogin);

module.exports = restaurantRouter;
