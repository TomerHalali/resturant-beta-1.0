const mongoose = require("mongoose");

const mealsSchema = new mongoose.Schema({
    id: String,
    title: String,
    affordability: String,
    complexity: String,
    imageUrl: String,
    duration: Number,
    ingredients: Array,
    steps: Array,
    eventOnly: Array
  });
module.exports = Meals = mongoose.model("Meals", mealsSchema);