const mongoose = require("mongoose");

const receiptsSchema = new mongoose.Schema({
  logoReceipt: String,
  numberId: String,
});

module.exports = Receipt = mongoose.model("Receipt", receiptsSchema);