const mongoose = require("mongoose");

const productsSchema = new mongoose.Schema({
    id: String,
    ownerId: Number,
    title: String,
    imageUrl: String,
    description: String,
    price: Number,
    whatInside: String,
  });
  
module.exports = Products = mongoose.model("Products", productsSchema);