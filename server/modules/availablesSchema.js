const mongoose = require("mongoose");

const availablesSchema = new mongoose.Schema({
  delivery: Boolean,
  pickup: Boolean,
});

module.exports = Availables = mongoose.model("Availables", availablesSchema);
