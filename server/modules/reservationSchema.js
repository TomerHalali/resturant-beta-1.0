const mongoose = require("mongoose");

const reservationSchema = new mongoose.Schema({
    nId: {
      type: Number,
      unique: true,
    },
    fullName: String,
    street: String,
    apaNumber: Number,
    floor: Number,
    entarece: String,
    apartment: Number,
    phoneNumber: Number,
    email: String,
    status: String,
    theOrder: Object,
    // options: {
    //   type: Number,
    //   default: 4,
    //   unique: true,
    //   required: [true, 'Must have a number']
    // }
  });
  
  module.exports = Reservation = mongoose.model("Reservation", reservationSchema);