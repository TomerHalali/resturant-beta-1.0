const mongoose = require("mongoose");

const usersSchema = new mongoose.Schema({
  userName: String,
  userPassword: String,
});

module.exports = Users = mongoose.model("Users", usersSchema);
