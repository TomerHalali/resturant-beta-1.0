module.exports = (temp, reservation) => {
  let output = temp.replace(/{%STREET%}/g, reservation.street);
  output = output.replace(/{%APANUMBER%}/g, reservation.apaNumber);
  output = output.replace(/{%ENTARECE%}/g, reservation.entarece);
  output = output.replace(/{%FLOOR%}/g, reservation.floor);
  output = output.replace(/{%APARTMENT%}/g, reservation.apartment);
  output = output.replace(/{%NID%}/g, reservation.nId);
  output = output.replace(/{%PRICE%}/g, reservation.theOrder.totalAmount);
  output = output.replace(/{%PHONE%}/g, reservation.phoneNumber);
  output = output.replace(/{%FULLNAME%}/g, reservation.fullName);
  output = output.replace(/{%EMAIL%}/g, reservation.email);
  output = output.replace(/{%LOGO%}/g, reservation.logo);
  output = output.replace(/{%IDRECEIPT%}/g, reservation.idReceipt);
  output = output.replace(/{%TIME%}/g, reservation.theOrder.id.substring(0,24));
  output = output.replace(/{%STATUS%}/g, reservation.status);
  output = output.replace(/{%CLOCK%}/g, reservation.theOrder.id.substring(16,24));

  var reservationDetails = "";
  for (var i = 0; i < reservation.theOrder.items.length; i++) {
    reservationDetails = reservationDetails.concat(
      `<b>`,
      reservation.theOrder.items[i].productTitle,
      `</b>`,
      "  :  ",
      reservation.theOrder.items[i].quantity,
      " | ",
      reservation.theOrder.items[i].whatInside,
      `<br>`,
      
    );
  }
  output = output.replace(/{%DESCRIPTION%}/g, reservationDetails);

  return output;
};
