const mongoose = require("mongoose");

const categoriesSchema = new mongoose.Schema({
    id: String,
    title: String,
    imageUrl: String
  });
module.exports = Categories = mongoose.model("Categories", categoriesSchema);