const mongoose = require("mongoose");

const htmlPagesSchema = new mongoose.Schema({
  title: String,
  htmlCode: String,
});

module.exports = HtmlPages = mongoose.model("HtmlPages", htmlPagesSchema);
