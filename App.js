import React from "react";

import { enableScreens } from "react-native-screens";
import { createStore, combineReducers } from "redux";
import { Provider } from "react-redux";

import MealsNavigator from "./navigation/MealsNavigator";
import mealsReducer from "./store/reducers/meals";

import cartReducer from "./store/reducers/cart";
import ordersReducer from "./store/reducers/orders";

enableScreens();

const rootReducer = combineReducers({
  meals: mealsReducer,
  cart: cartReducer,
  orders: ordersReducer,
});

const store = createStore(rootReducer);

export default function App() {

  return <Provider store={store}>{<MealsNavigator />}</Provider>;
}
