import React from "react";
import { Platform } from "react-native";
import { createDrawerNavigator } from "react-navigation-drawer";

import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import TextMenu from "../components/TextMenu";
import TextExpressMenu from "../components/TextExpressMenu";

import ContactUsScreen from "../screens/ContactUsScreen";
import DrinksScreen from "../screens/DrinksScreen";
import EventsMenuScreen from "../screens/EventsMenuScreen";
import BusinessScreen from "../screens/BusinessScreen";
import MainScreen from "../screens/MainScreen";
import AboutUsScreen from "../screens/AboutUsScreen";
import EveningScreen from "../screens/EveningScreen";
import EveningDetailsScreen from "../screens/EveningDetailsScreen";
import EventDetailsScreen from "../screens/EventDetailsScreen";
import BusinessDetailsScreen from "../screens/BusinessDetailsScreen";
import DessertsScreen from "../screens/DessertsScreen";
import MenuDeliveryScreen from "../screens/MenuDeliveryScreen";
import ExpressScreen from "../screens/ExpressScreen";
import CartScreen from "../screens/CartScreen";
import OrdersScreen from "../screens/OrdersScreen";
import FinishOrderScreen from "../screens/FinishOrderScreen";
import OrdersHistoryScreen from "../screens/OrdersHistoryScreen";

import DrinksDetailsScreen from "../screens/DrinksDetailsScreen";
import deliveryScreen from "../screens/deliveryScreen";
import PickupDetailsScreen from "../screens/PickupDetailsScreen";

import CodeInputScreen from "../screens/CodeInputScreen";

const defaultStackNavOptions = {
  //for any screen
  headerStyle: {
    backgroundColor: Platform.OS === "android" ? "black" : "black",
  },
  headerTitleStyle: {
    fontFamily: "open-sans-bold",
  },
  headerBackTitleStyle: {
    fontFamily: "open-sans-bold",
  },
  headerTintColor: Platform.OS === "android" ? "white" : "white",
};

////////////////////////////////////////Express Menu//////////////////////////////////////////////////

const MealsNavigatorExpress = createStackNavigator(
  {
    Express: ExpressScreen,
    deliveryScreen: {
      screen: deliveryScreen,
    },
    PickupDetailsScreen: {
      screen: PickupDetailsScreen,
    },
    menuDelivery: {
      screen: MenuDeliveryScreen,
    },
    Cart: CartScreen,
    Orders: OrdersScreen,
    valid: CodeInputScreen,
    finish: FinishOrderScreen,
  },

  {
    defaultNavigationOptions: defaultStackNavOptions,
  }
);

////////////////////////////////////////Desserts Menu/////////////////////////////////////////////////

const MealsNavigatorDesserts = createStackNavigator(
  {
    History: DessertsScreen,
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  }
);

////////////////////////////////////////History Menu/////////////////////////////////////////////////

const OrdersHistoryNavigation = createStackNavigator(
  {
    Desserts: OrdersHistoryScreen,
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  }
);

////////////////////////////////////////Evening Menu/////////////////////////////////////////////////
const MealsEveningMenu = createStackNavigator(
  {
    Categories: {
      screen: EveningScreen,
    },
    CategoryMeals: {
      screen: EveningDetailsScreen,
    },
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  }
);

////////////////////////////////////////BusinessMeal/////////////////////////////////////////////////

const MealsNavigatorBusinessMeal = createStackNavigator(
  {
    CategoryMeals: {
      screen: BusinessScreen,
    },
    MealDetail: BusinessDetailsScreen,
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  }
);

////////////////////////////////////////Drinks Menu/////////////////////////////////////////////////

const MealsNavigatorDrinksMenu = createStackNavigator(
  {
    CategoryMeals: {
      screen: DrinksScreen,
    },
    MealDetail: DrinksDetailsScreen,
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  }
);

////////////////////////////////////////Events Menu/////////////////////////////////////////////////

const MealsNavigatorEventsMenu = createStackNavigator(
  {
    CategoryMeals: {
      screen: EventsMenuScreen,
    },
    MealDetail: {
      screen: EventDetailsScreen,
    },
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  }
);

////////////////////////////////////////MainScreen/////////////////////////////////////////////////
const MyMainScreen = createStackNavigator(
  {
    Main: MainScreen,
    //MainScreen MenuDeliveryScreen ExpressScreen deliveryScreen
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  }
);

////////////////////////////////////////AboutUsScreen/////////////////////////////////////////////////
const MyAboutUsScreen = createStackNavigator(
  {
    AboutUs: AboutUsScreen,
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  }
);

////////////////////////////////////////Contact Us creen////////////////////////////////////////////

const MyContactScreen = createStackNavigator(
  {
    Contact: ContactUsScreen,
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  }
);

const MainNavigator = createDrawerNavigator(
  {
    Main: {
      screen: MyMainScreen,
      navigationOptions: {
        drawerLabel: <TextMenu title={"ראשי"} />,
      },
    },
    AboutUs: {
      screen: MyAboutUsScreen,
      navigationOptions: {
        drawerLabel: <TextMenu title={"קצת עלינו"} />,
      },
    },
    Express: {
      screen: MealsNavigatorExpress,
      navigationOptions: {
        drawerLabel: <TextExpressMenu title={"אקספרס ומשלוחים"} />,
      },
    },
    OrdersHistory: {
      screen: OrdersHistoryNavigation,
      navigationOptions: {
        drawerLabel: <TextExpressMenu title={"היסטורית הזמנות"} />,
      },
    },
    MealsFavs: {
      screen: MealsEveningMenu,
      navigationOptions: {
        drawerLabel: <TextMenu title={"תפריט ערב"} />,
      },
    },
    BusinessMeals: {
      screen: MealsNavigatorBusinessMeal,
      navigationOptions: {
        drawerLabel: <TextMenu title={"תפריט עסקיות"} />,
      },
    },
    EventsMenu: {
      screen: MealsNavigatorEventsMenu,
      navigationOptions: {
        drawerLabel: <TextMenu title={"תפריט אירועים"} />,
      },
    },
    Drinks: {
      screen: MealsNavigatorDrinksMenu,
      navigationOptions: {
        drawerLabel: <TextMenu title={"תפריט שתיה ואלכוהול"} />,
      },
    },
    Deserts: {
      screen: MealsNavigatorDesserts,
      navigationOptions: {
        drawerLabel: <TextMenu title={"תפריט קינוחים"} />,
      },
    },
    Contact: {
      screen: MyContactScreen,
      navigationOptions: {
        drawerLabel: <TextMenu title={"צור קשר ודרכי הגעה"} />,
      },
    },
  },

  {
    contentOptions: {
      activeBackgroundColor: "#ccc",
    },
    drawerPosition: "right",
    drawerWidth: "60%",
    drawerBackgroundColor: "#d3d3d3",
  }
);

export default createAppContainer(MainNavigator);
