import React from "react";
import { ScrollView, StyleSheet, Text, View, Image } from "react-native";
import Colors from '../constants/Colors';


import DefaultText from "../components/DefaultText";


const ListItem = (props) => {
  return (
    <View style={styles.listItem}>
      <Text style={styles.text} >{props.children}</Text>
    </View>
  );
};

const DrinksDetailsScreen = (props) => {

  const selectedMeal = props.navigation.getParam("item");

  return (
    <ScrollView style={{backgroundColor: 'black' }}>
      <Image source={{uri: selectedMeal.imageUrl}} style={styles.image} />
      <View style={styles.details}>
        <DefaultText>{selectedMeal.complexity.toUpperCase()}</DefaultText>
        <DefaultText>{selectedMeal.affordability.toUpperCase()}</DefaultText>
      </View>
      <Text style={styles.title}>סוגי שתיה</Text>
      {selectedMeal.ingredients.map((ingredient) => (
        <ListItem key={ingredient}>{ingredient}</ListItem>
      ))}

    </ScrollView>
  );
};

DrinksDetailsScreen.navigationOptions = (navigationData) => {
  const mealTitle = navigationData.navigation.getParam("item").title;

  return {
    headerTitle: mealTitle,
    headerTitleAlign: 'center',
  };
};

const styles = StyleSheet.create({
    image: {
      width: "100%",
      height: 200,
    },
    details: {
      flexDirection: "row",
      padding: 15,
      justifyContent: "space-around",
      backgroundColor: "black",
    },
    title: {
      fontSize: 14,
      textAlign: "center",
      backgroundColor: Colors.accentColor,
    },
    text:{
      color: 'white',
      fontSize: 10
    },
    listItem: {
      marginVertical: 2,
      marginHorizontal: 40,
      borderColor: "#ccc",
      borderWidth: 1,
      padding: 10,
      alignItems: 'center'
    },
  });

export default DrinksDetailsScreen;
