import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  Linking,
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import Colors from "../constants/Colors";
import { AntDesign } from "@expo/vector-icons";
import { Feather } from "@expo/vector-icons";
import { showLocation } from "react-native-map-link";
import I18n from "react-native-i18n";

const ContactUsScreen = (props) => {
  const showMapOnWaze = () => {
    showLocation({
      latitude: 31.2453757,
      longitude: 34.8130305,
      appsWhiteList: ["waze"], // optionally you can set which apps to show (default: will show all supported apps installed on device)
    });
  };

  const showMapOnGoogleMaps = () => {
    showLocation({
      latitude: 31.2453757,
      longitude: 34.8130305,
      appsWhiteList: ["google-maps"], // optionally you can set which apps to show (default: will show all supported apps installed on device)
    });
  };

  return (
    <ImageBackground
      source={require("../assets/mainBackground.png")}
      style={styles.bgImage}
    >
      <View style={{ flex: 1 }}>
        <View style={styles.screen}>
          <Text style={styles.title}>מסעדת אווז הזהב ביג באר שבע</Text>

          <Text style={styles.text}>דרך חברון 60 מרכז ביג באר שבע</Text>
        </View>

        <View style={styles.buttons}>
          <TouchableOpacity style={styles.buttonWaze} onPress={showMapOnWaze}>
            <Text style={{ color: "white" }}> נווט אלינו בעזרת Waze </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonGoogle}
            onPress={showMapOnGoogleMaps}
          >
            <Text style={{ color: "#f08080" }}>
              {" "}
              נווט אלינו בעזרת Google Maps{" "}
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.contact}>
          <AntDesign
            style={{ color: "white" }}
            onPress={() => {
              Linking.openURL(`tel:${"08-6287675"}`);
            }}
            name="phone"
            size={32}
          />

          <Feather
            style={{ color: "white" }}
            onPress={() => {
              Linking.openURL("https://www.facebook.com/avazhazahav");
            }}
            name="facebook"
            size={32}
          />

          <Feather
            style={{ color: "white" }}
            onPress={() => {
              Linking.openURL(
                "https://instagram.com/avaz_hazahav?igshid=1bxalr23x3l93"
              );
            }}
            name="instagram"
            size={32}
          />
        </View>
      </View>
    </ImageBackground>
  );
};

ContactUsScreen.navigationOptions = (navData) => {
  const language = I18n.currentLocale();
  return {
    headerTitle: "צור קשר",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
    headerRight: () => {
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  screen: {
    alignItems: "center",
  },
  touchable: {
    marginTop: "25%",
  },
  text: {
    color: "white",
    marginTop: "10%",
    marginTop: "30%",
  },
  title: {
    textAlign: "center",
    color: Colors.accentColor,
    fontSize: 20,
  },
  bgImage: {
    width: "100%",
    height: "100%",
    alignItems: "center",
  },
  contact: {
    justifyContent: "space-around",
    flexDirection: "row",
    flex: 1,
    marginVertical: 30,
  },
  buttons: {
    justifyContent: "space-between",
    marginHorizontal: 20,
  },
  buttonWaze: {
    alignItems: "center",
    backgroundColor: "#87ceeb",
    padding: 8,
    borderRadius: 3,
    marginVertical: 8,
  },
  buttonGoogle: {
    alignItems: "center",
    backgroundColor: "#8fbc8f",
    padding: 8,
    borderRadius: 3,
    marginVertical: 8,
  },
});

export default ContactUsScreen;
