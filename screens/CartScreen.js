import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ImageBackground,
  TouchableOpacity,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import CartItem from "../components/shop/CartItem";
import * as cartAction from "../store/actions/cart";
import * as orderActions from "../store/actions/orders";
import DialogComponentSpinner from "../components/DialogComponentSpinner";
import I18n from "react-native-i18n";
import { HeaderBackButton } from "react-navigation-stack";

const CartScreen = (props) => {
  const cartTotalAmount = useSelector((state) => state.cart.totalAmount);
  const cartItems = useSelector((state) => {
    const transformedCartItems = [];
    for (const key in state.cart.items) {
      transformedCartItems.push({
        productId: key,
        productTitle: state.cart.items[key].productTitle,
        productPrice: state.cart.items[key].productPrice,
        quantity: state.cart.items[key].quantity,
        sum: state.cart.items[key].sum,
        whatInside: state.cart.items[key].whatInside,
      });
    }

    return transformedCartItems.sort((a, b) =>
      a.productId > b.productId ? 1 : -1
    );
  });

  const dispatch = useDispatch();

  const street = props.navigation.getParam("street");
  const apaNumber = props.navigation.getParam("apaNumber");
  const entarece = props.navigation.getParam("entarece");
  const floor = props.navigation.getParam("floor");
  const apartment = props.navigation.getParam("apartment");
  const fullName = props.navigation.getParam("fullName");
  const phoneNumber = props.navigation.getParam("phoneNumber");
  const email = props.navigation.getParam("email");

  const [spinner, setSpinner] = useState(false);

  const language = I18n.currentLocale();

  return (
    <ImageBackground
      source={require("../assets/Background.jpg")}
      style={styles.bgImage}
    >
      <View style={styles.screen}>
        <View style={styles.flatView}>
          <FlatList
            data={cartItems}
            keyExtractor={(item) => item.productId}
            renderItem={(itemData) => (
              <CartItem
                quantity={itemData.item.quantity}
                title={itemData.item.productTitle}
                amount={itemData.item.sum}
                whatInside={itemData.item.whatInside}
                daletable
                onRemove={() => {
                  dispatch(
                    cartAction.removeFromCart(
                      itemData.item.productId,
                      itemData.item.whatInside
                    )
                  );
                }}
              />
            )}
          />
        </View>
      </View>
      <View style={styles.containerSummary}>
        <View style={styles.summary}>
          {language != "en-US" ? (
            <Text style={styles.summaryText}>
              סה"כ: <Text style={styles.amount}> {cartTotalAmount} ₪</Text>
            </Text>
          ) : (
            <TouchableOpacity
              disabled={cartItems.length === 0}
              onPress={() => {
                setSpinner(true);
                setTimeout(() => {
                  setSpinner(false);
                  dispatch(orderActions.addOrder(cartItems, cartTotalAmount)) &&
                    props.navigation.navigate("Orders", {
                      street: street,
                      apaNumber: apaNumber,
                      entarece: entarece,
                      floor: floor,
                      apartment: apartment,
                      phoneNumber: phoneNumber,
                      fullName: fullName,
                      email: email,
                    });
                }, 1800);
              }}
            >
              <Text style={styles.summaryText}>בצע הזמנה</Text>
            </TouchableOpacity>
          )}
          {spinner && <DialogComponentSpinner />}

          {language != "en-US" ? (
            <TouchableOpacity
              disabled={cartItems.length === 0}
              onPress={() => {
                setSpinner(true);
                setTimeout(() => {
                  setSpinner(false);
                  dispatch(orderActions.addOrder(cartItems, cartTotalAmount)) &&
                    props.navigation.navigate("Orders", {
                      street: street,
                      apaNumber: apaNumber,
                      entarece: entarece,
                      floor: floor,
                      apartment: apartment,
                      phoneNumber: phoneNumber,
                      fullName: fullName,
                      email: email,
                    });
                }, 1800);
              }}
            >
              <Text style={styles.summaryText}>בצע הזמנה</Text>
            </TouchableOpacity>
          ) : (
            <Text style={styles.summaryText}>
              סה"כ: <Text style={styles.amount}> {cartTotalAmount} ₪</Text>
            </Text>
          )}
        </View>
      </View>
    </ImageBackground>
  );
};

CartScreen.navigationOptions = (navData) => {
  const language = I18n.currentLocale();

  return {
    headerTitle: "ההזמנה שלי",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language == "en-US") {
        return (
          <HeaderBackButton
            tintColor="white"
            onPress={() => {
              navData.navigation.goBack();
            }}
          />
        );
      }
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
    headerRight: () => {
      if (language != "en-US") {
        return (
          <HeaderBackButton
            tintColor="white"
            backImage={() => (
              <Text style={{ color: "white", fontWeight: "bold" }}>חזור</Text>
            )}
            onPress={() => {
              navData.navigation.goBack();
            }}
          />
        );
      }
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  bgImage: {
    width: "100%",
    height: "100%",
    flex: 1,
  },
  screen: {
    flex: 1,
  },
  flatView: {
    padding: 20,
    borderRadius: 25,
  },
  summary: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10,
    borderRadius: 25,
    backgroundColor: "white",
  },
  summaryText: {
    fontSize: 16,
    color: "#a8883b",
  },
  amount: {
    color: "black",
  },
  text: {
    color: "white",
    fontSize: 18,
  },
  containerSummary: {
    flex: 0,
    padding: 20,
    justifyContent: "flex-end",
  },
});

export default CartScreen;
