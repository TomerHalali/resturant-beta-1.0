import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ImageBackground,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import { useSelector } from "react-redux";
import OrderItem from "../components/shop/OrderItem";
import * as postFetch from "../server/fetchs/postFetchs";
import DialogComponentSpinner from "../components/DialogComponentSpinner";
import I18n from "react-native-i18n";
import { HeaderBackButton } from "react-navigation-stack";

const OrdersScreen = (props) => {
  const [spinner, setSpinner] = useState(false);

  const orders = useSelector((state) => state.orders.orders);

  const street = props.navigation.getParam("street");
  const apaNumber = props.navigation.getParam("apaNumber");
  const entarece = props.navigation.getParam("entarece");
  const floor = props.navigation.getParam("floor");
  const apartment = props.navigation.getParam("apartment");
  const phoneNumber = props.navigation.getParam("phoneNumber");
  const fullName = props.navigation.getParam("fullName");
  const email = props.navigation.getParam("email");

  const ordersObj = orders[0];

  return (
    <ImageBackground
      source={require("../assets/Background.jpg")}
      style={styles.bgImage}
    >
      <View style={styles.screen}>
        <View style={styles.paymentArea}>
          <TextInput
            style={styles.inputStyle}
            textAlign="center"
            maxLength={16}
            placeholder="מספר כרטיס אשראי"
            placeholderTextColor="white"
            keyboardType="decimal-pad"
          />
          <View style={styles.twoInput}>
            <TextInput
              style={styles.inputStyleRow}
              textAlign="center"
              placeholder="CCV"
              maxLength={3}
              placeholderTextColor="white"
              keyboardType="decimal-pad"
            />
            <TextInput
              style={styles.inputStyleRow}
              textAlign="center"
              placeholder="תוקף"
              maxLength={4}
              placeholderTextColor="white"
              keyboardType="decimal-pad"
            />
          </View>
        </View>

        <View style={styles.items}>
          <FlatList
            data={orders}
            keyExtractor={(item) => item.id}
            renderItem={(itemData) => (
              <OrderItem
                amount={itemData.item.totalAmount}
                date={itemData.item.readableDate}
                items={itemData.item.items}
                fullName={fullName}
                phoneNumber={phoneNumber}
                street={street}
              />
            )}
          />
        </View>
        {spinner && <DialogComponentSpinner />}
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              setSpinner(true);
              setTimeout(() => {
                setSpinner(false);

                ////////send phone message validation
                postFetch.sendValidationCodeToThePhone(
                  "".concat(
                    "+972",
                    props.navigation
                      .getParam("phoneNumber")
                      .toString()
                      .substring(1, 10)
                  )
                );

                props.navigation.navigate("valid", {
                  street: street,
                  apaNumber: apaNumber,
                  entarece: entarece,
                  floor: floor,
                  apartment: apartment,
                  phoneNumber: phoneNumber,
                  fullName: fullName,
                  email: email,
                  ordersObj: ordersObj,
                });
              }, 1800);
            }}
          >
            <Text style={styles.textOnButtons}>שליחת הזמנה</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ImageBackground>
  );
};

OrdersScreen.navigationOptions = (navData) => {
  const language = I18n.currentLocale();

  return {
    headerTitle: "אישור הזמנה ותשלום",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language == "en-US") {
        return (
          <HeaderBackButton
            tintColor="white"
            onPress={() => {
              navData.navigation.goBack();
            }}
          />
        );
      }
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
    headerRight: () => {
      if (language != "en-US") {
        return (
          <HeaderBackButton
            tintColor="white"
            backImage={() => (
              <Text style={{ color: "white", fontWeight: "bold" }}>חזור</Text>
            )}
            onPress={() => {
              navData.navigation.goBack();
            }}
          />
        );
      }
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  bgImage: {
    width: "100%",
    height: "100%",
  },
  screen: {
    flex: 1,
  },
  items: {
    height: "50%",
    flex: 1,
  },
  paymentArea: {
    height: "15%",
  },
  buttonContainer: {
    alignItems: "center",
    justifyContent: "center",
    height: "15%",
  },
  button: {
    backgroundColor: "#a8883b",
    width: "55%",
    height: "38%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
  },
  textOnButtons: {
    fontSize: 14,
    color: "white",
  },

  inputStyle: {
    borderColor: "white",
    borderWidth: 1,
    borderRadius: 25,
    height: "45%",
    marginHorizontal: "20%",
    color: "white",
  },
  inputStyleRow: {
    width: "30%",
    height: "70%",
    marginTop: 4,
    borderColor: "white",
    borderWidth: 1,
    borderRadius: 25,
    height: "65%",
    color: "white",
  },
  twoInput: {
    justifyContent: "space-around",
    flexDirection: "row",
    marginHorizontal: "20%",
  },
});

export default OrdersScreen;
