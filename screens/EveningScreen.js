import React from "react";
import {
  FlatList,
  StyleSheet,
  ImageBackground,
  Text,
  View,
} from "react-native";

import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";

import CategoryGridTile from "../components/CategoryGridTile";
import { useSelector } from "react-redux";

import I18n from "react-native-i18n";

const EveningScreen = (props) => {
  let categoriesState = useSelector((state) => state.meals.categories.All);

  const renderGridItem = (itemData) => {
    return (
      <CategoryGridTile
        title={itemData.item.title}
        image={itemData.item.image}
        onSelect={() => {
          props.navigation.navigate({
            routeName: "CategoryMeals",
            params: {
              categoryTitle: itemData.item.title,
              // categoriesState: categoriesState
            },
          });
        }}
      />
    );
  };

  if (useSelector((state) => state.meals.categories.All == undefined)) {
    return (
      <ImageBackground
        source={require("../assets/Background.jpg")}
        style={styles.bgImage}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            marginHorizontal: 30,
          }}
        >
          <Text style={{ color: "white", fontSize: 26, textAlign: "center" }}>
            יש לטעון את האפליקציה מחדש ולודא חיבור לאינטרנט
          </Text>
        </View>
      </ImageBackground>
    );
  } else {
    return (
      <FlatList
        keyExtractor={(item, index) => item.id}
        data={categoriesState}
        renderItem={renderGridItem}
        numColumns={2}
        bounces={false}
      />
    );
  }
};

EveningScreen.navigationOptions = (navData) => {
  const language = I18n.currentLocale();
  return {
    headerTitle: "תפריט ערב",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => navData.navigation.toggleDrawer()}
            />
          </HeaderButtons>
        );
      }
    },
    headerRight: () => {
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => navData.navigation.toggleDrawer()}
            />
          </HeaderButtons>
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  bgImage: {
    width: "100%",
    height: "100%",
    flex: 1,
  },
});

export default EveningScreen;
