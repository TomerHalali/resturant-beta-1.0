import React from "react";
import { useSelector } from "react-redux";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import MealList from "../components/MealList";
import { StyleSheet, View, Text, ImageBackground } from "react-native";
import I18n from "react-native-i18n";

const BusinessScreen = (props) => {
  const catTitle = "עסקיות";

  const availableMeals = useSelector((state) => state.meals.meals.All);
  var displayedMeals = [];
  if (useSelector((state) => state.meals.meals.All != undefined)) {
    displayedMeals = availableMeals.filter(
      (meal) => meal.id.indexOf(catTitle) >= 0
    );
  }

  if (useSelector((state) => state.meals.meals.All == undefined)) {
    return (
      <ImageBackground
        source={require("../assets/Background.jpg")}
        style={styles.bgImage}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            marginHorizontal: 30,
          }}
        >
          <Text style={{ color: "white", fontSize: 26, textAlign: "center" }}>
            יש לטעון את האפליקציה מחדש ולודא חיבור לאינטרנט
          </Text>
        </View>
      </ImageBackground>
    );
  } else {
    return <MealList listData={displayedMeals} navigation={props.navigation} />;
  }
};

BusinessScreen.navigationOptions = (navigationData) => {
  const language = I18n.currentLocale();
  return {
    headerTitle: "תפריט עסקיות",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navigationData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
    headerRight: () => {
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navigationData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "black",
  },
  bgImage: {
    width: "100%",
    height: "100%",
    flex: 1,
  },
});
export default BusinessScreen;
