import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  BackHandler,
  AsyncStorage,
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import DialogAlert from "../components/DialogAlert";
import Colors from "../constants/Colors";
import { useDispatch, useSelector } from "react-redux";
import * as mealAction from "../store/actions/meals";
import { NavigationActions } from "react-navigation";
import I18n from "react-native-i18n";

const MainScreen = (props) => {
  const dispatch = useDispatch();

  async function getAllCategories() {
    const responseA = await fetch(
      `https://stark-ravine-11171.herokuapp.com/getCategories`
    );
    const dataCategories = await responseA.json();
    dispatch(mealAction.getAllCategories(dataCategories.data.allCategories));
    console.log("Loading All Categories Data");
  }
  async function getAllMeals() {
    const responseB = await fetch(
      `https://stark-ravine-11171.herokuapp.com/getMeals`
    );
    const dataMeals = await responseB.json();
    dispatch(mealAction.getAllMenuMeals(dataMeals.data.allMeals));
    console.log("Loading All Meals Data");
  }
  async function getAllProducts() {
    const responseC = await fetch(
      `https://stark-ravine-11171.herokuapp.com/getProducts`
    );
    const dataProducts = await responseC.json();
    dispatch(mealAction.getAllExpressProducts(dataProducts.data.allProducts));
    console.log("Loading All Express Data");
  }

  useEffect(() => {
    //Get All Categories 23
    getAllCategories();
    //Get All Meals
    getAllMeals();
    //Get All Product Express
    getAllProducts();
    console.log(I18n.currentLocale());
  }, []);

  const handleOk = () => {
    setVisible(false);
    BackHandler.exitApp();
  };
  const handleCancle = () => {
    setVisible(false);
  };

  const [visible, setVisible] = useState(false);

  //Quit From The App - Message To The User
  useEffect(() => {
    const backAction = () => {
      if (props.navigation.isFocused()) {
        setVisible(true);
        return true;
      }
    };
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }, []);

  //////////////

  const clearAsyncStorage = async () => {
    AsyncStorage.clear();
  };

  return (
    <View style={styles.screen}>
      {/* <Text style={{ color: "white" }} onPress={clearAsyncStorage}>
        CLEAR
      </Text> */}
      <View style={styles.imageContainer}>
        <Image
          source={require("../assets/mainPicture.jpg")}
          style={styles.image}
        />
      </View>

      {visible && (
        <DialogAlert
          onOk={handleOk}
          onCancle={handleCancle}
          title={"יציאה"}
          message={"האם ברצונך לצאת מהאפליקציה?"}
          screen={"Main"}
        />
      )}
      <View style={styles.textContainer}>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate(
              NavigationActions.navigate({
                routeName: "Express",
                action: NavigationActions.navigate({
                  routeName: "MealsNavigatorExpress",
                }),
              })
            );
          }}
        >
          <Text style={styles.text}>אני רוצה להזמין {`  >`} </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

MainScreen.navigationOptions = (navData) => {
  const language = I18n.currentLocale();

  return {
    headerTitle: "ראשי",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
    headerRight: () => {
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  screen: {
    alignItems: "center",
    justifyContent: "flex-start",
    flex: 1,
    backgroundColor: "black",
  },
  image: {
    width: "100%",
    height: "100%",
  },
  imageContainer: {
    width: "100%",
    height: "70%",
    alignItems: "center",
  },
  text: {
    color: Colors.accentColor,
    fontSize: 30,
    fontWeight: "bold",
    fontFamily: "",
  },
  textContainer: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    flex: 1,
  },
});

export default MainScreen;
