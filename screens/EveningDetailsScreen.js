import React from "react";

import MealListEvening from "../components/MealListEvening";
import { StyleSheet } from "react-native";
import { useSelector } from "react-redux";


const EveningDetailsScreen = (props) => {


  const categoryTitle = props.navigation.getParam("categoryTitle");

  const availableMeals = useSelector(state => state.meals.meals.All);
  const displayedMeals = availableMeals.filter(
    (meal) => meal.id.indexOf(categoryTitle) >= 0
  );



  return <MealListEvening listData={displayedMeals} />;
};

EveningDetailsScreen.navigationOptions = (navigationData) => {
  const catTitle = navigationData.navigation.getParam("categoryTitle");

  return {
    headerTitle: catTitle,
    headerTitleAlign: 'center',
  };
};

const styles = StyleSheet.create({
  content:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black'

  }
});
export default EveningDetailsScreen;
