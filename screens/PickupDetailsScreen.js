import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  ImageBackground,
  View,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Alert,
  BackHandler,
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import Colors from "../constants/Colors";
import DialogAlert from "../components/DialogAlert";
import DialogComponentSpinner from "../components/DialogComponentSpinner";
import { HeaderBackButton } from "react-navigation-stack";
import I18n from "react-native-i18n";

const PickuoDetailsScreen = (props) => {
  const [visible, setVisible] = useState(false);
  const [spinner, setSpinner] = useState(false);

  function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  const apply = () => {
    if (fullNameIsValid && phoneNumberIsValid && emailIsValid) {
      setSpinner(true);
      setTimeout(() => {
        setSpinner(false);
        props.navigation.navigate("menuDelivery", {
          street: street,
          apaNumber: apaNumber,
          entarece: entarece,
          floor: floor,
          apartment: apartment,
          fullName: fullName,
          email: email,
          phoneNumber: phoneNumber,
        });
      }, 1800);
    } else {
      setVisible(true);
    }
  };

  const [fullName, setFullName] = useState("");
  const [street, setStreet] = useState("פיק-אפ");
  const [apaNumber, setApaNumber] = useState(0);
  const [entarece, setEntrace] = useState(0);
  const [floor, setFloor] = useState(0);
  const [apartment, setApartment] = useState(0);
  const [phoneNumber, setPhoneNumber] = useState(0);
  const [email, setEmail] = useState("");

  const [fullNameIsValid, setFullNameIsValid] = useState(false);
  const [phoneNumberIsValid, setPhoneNumberIsValid] = useState(false);
  const [emailIsValid, setEmailIsValid] = useState(false);

  const titleChangeHandler = (text) => {
    if (text.trim().length === 0) {
      setFullNameIsValid(false);
    } else {
      setFullNameIsValid(true);
    }
    setFullName(text);
  };

  const phoneNumberChangeHandler = (text) => {
    if (
      text.trim().length < 10 ||
      text[0] != 0 ||
      text[1] != 5 ||
      !Number.isInteger(parseInt(text[2])) ||
      !Number.isInteger(parseInt(text[3])) ||
      !Number.isInteger(parseInt(text[4])) ||
      !Number.isInteger(parseInt(text[5])) ||
      !Number.isInteger(parseInt(text[6])) ||
      !Number.isInteger(parseInt(text[7])) ||
      !Number.isInteger(parseInt(text[8])) ||
      !Number.isInteger(parseInt(text[9]))
    ) {
      setPhoneNumberIsValid(false);
    } else {
      setPhoneNumberIsValid(true);
    }
    setPhoneNumber(text);
  };

  const emailChangeHandler = (text) => {
    if (validateEmail(text)) {
      if (text.trim().length === 0) {
        setEmailIsValid(false);
      } else {
        setEmailIsValid(true);
      }
      setEmail(text);
    } else {
      setEmailIsValid(false);
    }
  };

  const handleCancle = () => {
    setVisible(false);
  };

  //Quit From The Currnt Page - Message To The User
  useEffect(() => {
    const backAction = () => {
      if (props.navigation.isFocused()) {
        Alert.alert(
          "חזרה לתחילת ההזמנה",
          "פעולה זו תמחק את פרטי ההזמנה הנוכחיים!",
          [
            {
              text: "ביטול",
              onPress: () => {},
              style: "cancel",
            },
            {
              text: "אישור",
              style: "destructive",
              onPress: () => props.navigation.goBack(),
            },
          ],
          { cancelable: false }
        );

        return true;
      }
    };
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }, []);

  return (
    <ImageBackground
      source={require("../assets/Background.jpg")}
      style={styles.bgImage}
    >
      <ScrollView contentContainerStyle={{ flex: 1 }}>
        <View style={styles.screen}>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.Input}
              textAlign="right"
              placeholder={"שם ושם משפחה"}
              maxLength={20}
              placeholderTextColor="white"
              onChangeText={titleChangeHandler}
            />
            <View>
              {!fullNameIsValid && <Text style={styles.valid}>*חובה</Text>}
            </View>

            <TextInput
              style={styles.Input}
              textAlign="right"
              placeholder={"טלפון"}
              placeholderTextColor="white"
              keyboardType="numeric"
              maxLength={10}
              onChangeText={phoneNumberChangeHandler}
            />
            <View>
              {!phoneNumberIsValid && (
                <Text style={styles.valid}>* מספר פלאפון לוידוא במסרון</Text>
              )}
            </View>

            <TextInput
              style={{ ...styles.Input, marginBottom: 4 }}
              textAlign="right"
              placeholder={"כתובת מייל"}
              placeholderTextColor="white"
              keyboardType="email-address"
              onChangeText={emailChangeHandler}
            />
            <View>
              {!emailIsValid && (
                <Text style={{ ...styles.valid, marginBottom: "5%" }}>
                  * אימייל לשליחת הקבלה
                </Text>
              )}
            </View>
          </View>
        </View>

        {visible && (
          <DialogAlert
            onOk={handleCancle}
            title={"מילוי טופס"}
            message={"נא למלא את כל השדות הנדרשים"}
          />
        )}
        {spinner && <DialogComponentSpinner />}

        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              apply();
            }}
          >
            <Text style={styles.text}>לתפריט המשלוחים</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </ImageBackground>
  );
};

PickuoDetailsScreen.navigationOptions = (navData) => {
  const language = I18n.currentLocale();

  return {
    headerTitle: "איסוף עצמי",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
      if (language == "en-US") {
        return (
          <HeaderBackButton
            tintColor="white"
            onPress={() => {
              Alert.alert(
                "חזרה לתחילת ההזמנה",
                "פעולה זו תמחק את פרטי ההזמנה הנוכחיים!",
                [
                  {
                    text: "ביטול",
                    onPress: () => {},
                    style: "cancel",
                  },
                  {
                    text: "אישור",
                    style: "destructive",
                    onPress: () => navData.navigation.goBack(),
                  },
                ],
                { cancelable: false }
              );
            }}
          />
        );
      }
    },
    headerRight: () => {
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
      if (language != "en-US") {
        return (
          <HeaderBackButton
            tintColor="white"
            backImage={() => (
              <Text style={{ color: "white", fontWeight: "bold" }}>חזור</Text>
            )}
            onPress={() => {
              Alert.alert(
                "חזרה לתחילת ההזמנה",
                "פעולה זו תמחק את פרטי ההזמנה הנוכחיים!",
                [
                  {
                    text: "ביטול",
                    onPress: () => {},
                    style: "cancel",
                  },
                  {
                    text: "אישור",
                    style: "destructive",
                    onPress: () => navData.navigation.goBack(),
                  },
                ],
                { cancelable: false }
              );
            }}
          />
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  spinnerView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
  },
  inputContainer: {
    alignItems: "center",
    borderColor: Colors.accentColor,
    borderWidth: 1,
    marginLeft: 6,
    marginRight: 6,
    marginTop: "4%",
  },
  bgImage: {
    width: "100%",
    height: "100%",
  },
  button: {
    backgroundColor: Colors.accentColor,
    borderRadius: 10,
    width: "50%",
    height: "15%",
    justifyContent: "center",
  },
  text: {
    color: "white",
    textAlign: "center",
  },
  Input: {
    width: "100%",
    marginLeft: "30%",
    marginRight: "30%",
    marginTop: "2%",
    borderBottomColor: Colors.accentColor,
    borderBottomWidth: 1,
    color: "white",
    padding: 6,
  },
  buttonContainer: {
    flex: 1,
    justifyContent: "flex-start",
    width: "100%",
    alignItems: "center",
    marginTop: "8%",
  },
  valid: {
    color: "red",
    fontSize: 12,
  },
});

export default PickuoDetailsScreen;
