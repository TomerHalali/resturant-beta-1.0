import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import Colors from "../constants/Colors";
import DialogComponentSpinner from "../components/DialogComponentSpinner";
import I18n from "react-native-i18n";

const ExpressScreen = (props) => {
  const [available, setAbailable] = useState({});
  async function getAvailableExpress() {
    const responseC = await fetch(
      `https://stark-ravine-11171.herokuapp.com/getAvailableExpress`
    );
    const dataProducts = await responseC.json();
    setAbailable(dataProducts.data[0]);
    console.log("Loading All Available Express Data");
  }

  useEffect(() => {
    //Get All Available Express
    getAvailableExpress();
    console.log(available);
  }, []);
  const [visible, setVisible] = useState(false);
  return (
    <ImageBackground
      source={require("../assets/Background.jpg")}
      style={styles.bgImage}
    >
      <View style={styles.details}>
        {!available.pickup && (
          <Text style={{ color: "red" }}>עמכם הסליחה,</Text>
        )}
        {!available.delivery && (
          <Text style={{ color: "red" }}>נכון לעכשיו משלוחים אינו זמין</Text>
        )}
        {!available.pickup && (
          <Text style={{ color: "red" }}>נכון לעכשיו איסוף עצמי אינו זמין</Text>
        )}
      </View>
      <View style={styles.theMainView}>
        <TouchableOpacity
          disabled={!available.delivery}
          style={styles.button}
          onPress={() => {
            setVisible(true);
            setTimeout(() => {
              setVisible(false);
              props.navigation.navigate({
                routeName: "deliveryScreen",
              });
            }, 1800);
          }}
        >
          <Text style={styles.text}>משלוח</Text>
        </TouchableOpacity>
        <View style={styles.details}>
          <Text style={{ color: "#a8883b" }}>
            משלוחים יבוצעו בין השעות 11 בבוקר ועד 21 בערב
          </Text>
        </View>
        {visible && <DialogComponentSpinner />}
        <TouchableOpacity
          disabled={!available.pickup}
          style={{ ...styles.button, margin: 60 }}
          onPress={() => {
            setVisible(true);
            setTimeout(() => {
              setVisible(false);
              props.navigation.navigate({
                routeName: "PickupDetailsScreen",
              });
            }, 1800);
          }}
        >
          <Text style={styles.text}>איסוף עצמי</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

ExpressScreen.navigationOptions = (navData) => {
  const language = I18n.currentLocale();
  return {
    headerTitle: "אקספרס ומשלוחים",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
    headerRight: () => {
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  bgImage: {
    width: "100%",
    height: "100%",
  },
  theMainView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: "5%",
  },
  spinnerView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
  },
  text: {
    color: "white",
    textAlign: "center",
  },
  button: {
    backgroundColor: Colors.accentColor,
    borderRadius: 10,
    width: "50%",
    height: "7%",
    justifyContent: "center",
  },
  details: {
    marginTop: "3%",
    alignItems: "center",
  },
});

export default ExpressScreen;
