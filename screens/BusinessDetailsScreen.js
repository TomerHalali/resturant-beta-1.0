import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
} from "react-native";

import Colors from "../constants/Colors";
import { useSelector} from "react-redux";
import DefaultText from "../components/DefaultText";


const ListItem = (props) => {
  return (
    <View style={styles.listItem}>
      <DefaultText>{props.children}</DefaultText>
    </View>
  );
};

const BusinessDetailsScreen = (props) => {

  const mealItem = props.navigation.getParam("item");
  const availableMeals = useSelector((state) => state.meals.meals.All);
  const mealTitle = mealItem.title;


  const selectedMeal = availableMeals.find((meal) => meal.title === mealTitle);


  return (
    <ScrollView style={{ backgroundColor: "black" }}>
      <Image source={{uri : selectedMeal.imageUrl }} style={styles.image} />
      <View style={styles.details}>
        <DefaultText>{selectedMeal.complexity.toUpperCase()}</DefaultText>
        <DefaultText>{selectedMeal.affordability.toUpperCase()}</DefaultText>
      </View>
      <Text style={styles.title}>על השולחן</Text>

      {selectedMeal.ingredients.map((ingredient) => (
        <ListItem key={ingredient}>{ingredient}</ListItem>
      ))}

      <Text style={styles.title}>תוספות לבחירה</Text>
      {selectedMeal.steps.map((steps) => (
        <ListItem key={steps}>{steps}</ListItem>
      ))}

      <Text style={styles.title}>מנה בשרית לבחירה</Text>
      {selectedMeal.eventOnly.map((eventOnly) => (
        <ListItem key={eventOnly}>{eventOnly}</ListItem>
      ))}
    </ScrollView>
  );
}

BusinessDetailsScreen.navigationOptions = (navigationData) => {
  const mealTitle = navigationData.navigation.getParam("item").title;

  return {
    headerTitle: mealTitle,
    headerTitleAlign: "center",
  };
};

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 200,
  },
  details: {
    flexDirection: "row",
    padding: 15,
    justifyContent: "space-around",
    backgroundColor: "black",
  },
  title: {
    fontSize: 14,
    textAlign: "center",
    backgroundColor: Colors.accentColor,
  },
  listItem: {
    marginVertical: 2,
    marginHorizontal: 40,
    borderColor: "#ccc",
    borderWidth: 1,
    padding: 10,
    alignItems: 'center'
  },
});

export default BusinessDetailsScreen;
