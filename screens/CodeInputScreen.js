import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  ScrollView,

} from "react-native";
import CodeInput from "react-native-code-input";
import { StackActions } from "react-navigation";
import * as cartActions from "../store/actions/cart";
import * as orderActions from "../store/actions/orders";
import { useDispatch } from "react-redux";
import DialogAlert from "../components/DialogAlert";
import DialogComponentSpinner from "../components/DialogComponentSpinner";

import * as postFetch from "../server/fetchs/postFetchs";

const CodeInputContainer = (props) => {
  const dispatch = useDispatch();

  const [spinner, setSpinner] = useState(false);

  const street = props.navigation.getParam("street");
  const apaNumber = props.navigation.getParam("apaNumber");
  const entarece = props.navigation.getParam("entarece");
  const floor = props.navigation.getParam("floor");
  const apartment = props.navigation.getParam("apartment");
  const fullName = props.navigation.getParam("fullName");
  const email = props.navigation.getParam("email");
  const ordersObj = props.navigation.getParam("ordersObj");

  const [itsValid, setItsValid] = useState("");

  const [visibleCode, setVisibleCode] = useState(false);
  const [visibleSendAgain, setVisibleSendAgain] = useState(false);

  const [phoneNumber, setPhoneNumber] = useState(
    "".concat(
      "+972",
      props.navigation.getParam("phoneNumber").toString().substring(1, 10)
    )
  );

  const phoneToShow = "";

  const sendFetch = (code) => {
    fetch(`https://stark-ravine-11171.herokuapp.com/sendVerifyCodeMessage`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        phoneNumber: phoneNumber,
        code: code,
      }),
    })
      .then((response) => response.json())
      .then((responseData) => {
        setItsValid(responseData.itsValid);
      })
      .done();
  };

  const [seconds, setSeconds] = useState(60);
  const [isActive, setIsActive] = useState(true);

  function reset() {
    setSeconds(60);
    setIsActive(true);
  }

  useEffect(() => {
    let interval = null;
    if (seconds == 0) {
      clearInterval(interval);
      setIsActive(false);
    } else if (isActive) {
      interval = setInterval(() => {
        setSeconds((seconds) => seconds - 1);
      }, 1000);
    } else if (!isActive && seconds !== 0) {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [isActive, seconds]);

  const handleCancle = () => {
    setVisibleCode(false);
    setVisibleSendAgain(false);
  };




  return (
    <ImageBackground
      source={require("../assets/mainBackground.png")}
      style={styles.bgImage}
    >
      <ScrollView contentContainerStyle={styles.container}>
        <Text style={styles.title}>אימות ב-SMS</Text>
        <Text style={styles.subTitle}>הכנס את הקוד שקיבלת לנייד שמספרו</Text>

        <Text style={styles.subTitle}>
          {phoneToShow.concat(0, phoneNumber.substring(4, 13))}
        </Text>

        <View style={styles.codeInputContainer}>
          <CodeInput
            keyboardType="numeric"
            codeLength={6}
            borderType="circle"
            activeColor="#a8883b"
            inactiveColor="white"
            cellBorderWidth={1.5}
            autoFocus={false}
            onFulfill={(code) => sendFetch(code)}
            size={50}
            space={5}
          />
        </View>
        {spinner && <DialogComponentSpinner />}
        <View style={styles.textContainer}>
          {!isActive && (
            <TouchableOpacity
              onPress={() => {
                postFetch.sendValidationCodeToThePhone(phoneNumber);
                setVisibleSendAgain(true);
                reset();
              }}
            >
              <Text style={{ ...styles.text, color: "red" }}>שלח שוב </Text>
            </TouchableOpacity>
          )}
          <Text style={{ color: "white", fontSize: 18 }}>
            {seconds != 0 && seconds}
          </Text>
          <Text style={styles.text}>לא קיבלת קוד? </Text>
        </View>

        {visibleCode && (
          <DialogAlert
            onOk={handleCancle}
            title={"שגיאה בקוד"}
            message={"הכנס קוד אימות"}
          />
        )}
        {visibleSendAgain && (
          <DialogAlert
            onOk={handleCancle}
            title={"קוד נשלח שוב"}
            message={"אנא אמתן מספר שניות"}
          />
        )}

        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            if (itsValid == "approved") {
              setSpinner(true);
              setTimeout(() => {
                setSpinner(false);
                ////////send phone message validation
                fetch(
                  `https://stark-ravine-11171.herokuapp.com/newReservation`,
                  {
                    method: "POST",
                    headers: {
                      Accept: "application/json",
                      "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                      fullName: fullName,
                      street: street,
                      apaNumber: apaNumber,
                      entarece: entarece,
                      floor: floor,
                      apartment: apartment,
                      phoneNumber: props.navigation.getParam("phoneNumber"),
                      email: email,
                      status: "התקבלה",
                      theOrder: ordersObj,
                    }),
                  }
                );


                dispatch(cartActions.resetTheCart());
                dispatch(orderActions.resetOrders());
                props.navigation.dispatch(StackActions.popToTop());
                props.navigation.navigate("finish", {
                  email: email,
                  fullName: fullName,
                  orderToSave: JSON.stringify({
                    fullName: fullName,
                    street: street,
                    apaNumber: apaNumber,
                    entarece: entarece,
                    floor: floor,
                    apartment: apartment,
                    phoneNumber: props.navigation.getParam("phoneNumber"),
                    email: email,
                    status: "התקבלה",
                    theOrder: ordersObj,
                  }),
                });
              }, 1800);
            } else {
              setVisibleCode(true);
            }
          }}
        >
          <Text style={styles.text}>שליחת הזמנה</Text>
        </TouchableOpacity>
      </ScrollView>
    </ImageBackground>
  );
};

CodeInputContainer.navigationOptions = (navData) => {
  return {
    headerTitle: "אימות",
    headerTitleAlign: "center",
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: 3,
    alignItems: "center",
    marginTop: "20%",
  },
  title: {
    textAlign: "center",
    fontSize: 40,
    color: "white",
  },
  subTitle: {
    textAlign: "center",
    fontSize: 20,
    color: "white",
  },
  text: {
    textAlign: "right",
    fontSize: 18,
    color: "white",
  },
  codeInputContainer: {
    height: "20%",
  },
  textContainer: {
    justifyContent: "flex-end",
    flexDirection: "row",
  },
  bgImage: {
    width: "100%",
    height: "100%",
  },
  button: {
    backgroundColor: "#a8883b",
    borderRadius: 10,
    width: "50%",
    height: "7%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "5%",
  },
  valid: {
    color: "red",
    fontSize: 12,
    textAlign: "center",
  },
  Input: {
    borderBottomColor: "#a8883b",
    borderBottomWidth: 1,
    color: "white",
    textAlign: "center",
  },
  phoneInputContainer: {
    width: "80%",
    height: "12%",
  },
});

export default CodeInputContainer;
