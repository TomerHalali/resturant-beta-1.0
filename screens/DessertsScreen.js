import React from "react";
import {
  FlatList,
  StyleSheet,
  ImageBackground,
  View,
  Text,
} from "react-native";

import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import DessertsComponent from "../components/DessertsComponent";
import { useSelector } from "react-redux";
import I18n from "react-native-i18n";

const MealDessertsScreen = (props) => {
  const catId = "קינוחים";
  const availableMeals = useSelector((state) => state.meals.meals.All);

  var displayedMeals = [];

  if (useSelector((state) => state.meals.meals.All != undefined)) {
    displayedMeals = availableMeals.filter(
      (meal) => meal.id.indexOf(catId) >= 0
    );
  }
  const renderGridItem = (itemData) => {
    return (
      <DessertsComponent
        title={itemData.item.title}
        imageUrl={itemData.item.imageUrl}
      />
    );
  };

  if (useSelector((state) => state.meals.meals.All == undefined)) {
    return (
      <ImageBackground
        source={require("../assets/Background.jpg")}
        style={styles.bgImage}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            marginHorizontal: 30,
          }}
        >
          <Text style={{ color: "white", fontSize: 26, textAlign: "center" }}>
            יש לטעון את האפליקציה מחדש ולודא חיבור לאינטרנט
          </Text>
        </View>
      </ImageBackground>
    );
  } else {
    return (
      <View style={styles.screen}>
        <FlatList
          keyExtractor={(item, index) => item.id}
          data={displayedMeals}
          renderItem={renderGridItem}
          numColumns={2}
          bounces={false}
        />
      </View>
    );
  }
};

MealDessertsScreen.navigationOptions = (navData) => {
  const language = I18n.currentLocale();
  return {
    headerTitle: "תפריט קינוחים",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => navData.navigation.toggleDrawer()}
            />
          </HeaderButtons>
        );
      }
    },
    headerRight: () => {
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => navData.navigation.toggleDrawer()}
            />
          </HeaderButtons>
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "black",
  },
  bgImage: {
    width: "100%",
    height: "100%",
    flex: 1,
  },
});

export default MealDessertsScreen;
