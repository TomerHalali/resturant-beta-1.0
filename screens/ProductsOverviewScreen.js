import React from "react";
import { View, Text } from "react-native";
import { FlatList } from "react-native";

import { useSelector, useDispatch } from "react-redux";

import ProductItem from "../components/shop/ProductItem";
import * as cartActions from "../store/actions/cart";

const ProductsOverviewScreen = (props) => {
  const dispatch = useDispatch();
  var products = [];

  if (useSelector((state) => state.meals.expressProducts.All == undefined)) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          marginHorizontal: 30,
        }}
      >
        <Text style={{ color: "white", fontSize: 26 , textAlign:'center'}}>
          יש לטעון את האפליקציה מחדש ולודא חיבור לאינטרנט
        </Text>
      </View>
    );
  } else {
    products = useSelector((state) =>
      state.meals.expressProducts.All.filter(
        (prod) => prod.ownerId === props.selectedTab
      )
    );

    return (
      <View>
        <FlatList
          style={{ marginBottom: "9%" }}
          data={products}
          keyExtractor={(item) => item.id}
          renderItem={(itemData) => (
            <ProductItem
              selectedTab={props.selectedTab}
              image={itemData.item.imageUrl} //
              title={itemData.item.title}
              price={itemData.item.price}
              whatInside={itemData.item.whatInside}
              onAddToCart={(QUT, whatToPutInsideArr) => {
                dispatch(
                  cartActions.addToCart(itemData.item, QUT, whatToPutInsideArr)
                );
              }}
            />
          )}
        />
      </View>
    );
  }
};

export default ProductsOverviewScreen;
