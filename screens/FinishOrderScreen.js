import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage,
} from "react-native";
import { NavigationActions, StackActions } from "react-navigation";

const FinishOrderScreen = (props) => {
  const email = props.navigation.getParam("email");
  const fullName = props.navigation.getParam("fullName");
  const orderToSave = props.navigation.getParam("orderToSave");

  const storeData = async () => {
    try {
      const keys = await AsyncStorage.getAllKeys();
      var key = Math.random();

      for (var i = 0; i < keys.length; i++) {
        if (keys[i] == key) {
          key = Math.random();
          i = 0;
        }
      }
      key = key.toString();
      AsyncStorage.setItem(key, orderToSave);
      console.log("Stored");
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    storeData();
  }, []);

  return (
    <ImageBackground
      source={require("../assets/mainBackground.png")}
      style={styles.bgImage}
    >
      <View style={styles.textContainer}>
        <Text style={styles.mainText}>{fullName} תודה</Text>
        <Text style={styles.mainText}>הזמנתך בוצעה בהצלחה!</Text>
        <Text style={styles.mainText}>קבלה נשלחה למייל</Text>
        <Text style={styles.email}>{email}</Text>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            props.navigation.dispatch(StackActions.popToTop());
            props.navigation.navigate(
              NavigationActions.navigate({
                routeName: "Main",
                action: NavigationActions.navigate({
                  routeName: "MyMainScreen",
                }),
              })
            );
          }}
        >
          <Text style={styles.textButton}>סיום</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

FinishOrderScreen.navigationOptions = (navData) => {
  return {
    headerTitle: "סיום הזמנה",
    headerTitleAlign: "center",
  };
};

const styles = StyleSheet.create({
  bgImage: {
    width: "100%",
    height: "100%",
  },
  button: {
    backgroundColor: "#a8883b",
    borderRadius: 10,
    width: "50%",
    height: "12%",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonContainer: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
  },
  textContainer: {
    flex: 1,
    justifyContent: "center",
  },
  mainText: {
    textAlign: "center",
    fontSize: 24,
    color: "white",
  },
  email: {
    textAlign: "center",
    fontSize: 16,
    color: "white",
  },
  textButton: {
    color: "white",
  },
});

export default FinishOrderScreen;
