import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";
import {
  StyleSheet,
  ImageBackground,
  ScrollView,
  AsyncStorage,
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import { NavigationEvents } from "react-navigation";
import HistoryBlock from "../components/HistoryBlock";
import I18n from "react-native-i18n";

const OrderHistoryScreen = (props) => {
  const [AllTheReservations, setAllTheReservations] = useState([]);

  const retrieveData = async () => {
    try {
      const keys = await AsyncStorage.getAllKeys();
      const result = await AsyncStorage.multiGet(keys);
      var value = [];

      for (var i = 0; i < keys.length; i++) {
        value = value.concat(JSON.parse(await AsyncStorage.getItem(keys[i])));
      }

      setAllTheReservations(value);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    retrieveData();
  }, []);

  if (AllTheReservations.length == 0) {
    return (
      <ImageBackground
        source={require("../assets/Background.jpg")}
        style={styles.bgImage}
      >
        <View
          style={{ justifyContent: "center", alignItems: "center", flex: 1 }}
        >
          <Text style={{ color: "#a8883b", fontSize: 26 }}>
            אין היסטורית הזמנות
          </Text>
          <NavigationEvents
            onWillFocus={() => {
              retrieveData();
            }}
          />
        </View>
      </ImageBackground>
    );
  }

  return (
    <ImageBackground
      source={require("../assets/Background.jpg")}
      style={styles.bgImage}
    >
      <ScrollView>
        <NavigationEvents
          onWillFocus={() => {
            retrieveData();
          }}
        />
        <View style={styles.screen}>
          {AllTheReservations.map((reservation) => (
            <HistoryBlock
              key={Math.random()}
              items={reservation.theOrder.items}
              sum={reservation.theOrder.totalAmount}
              date={reservation.theOrder.id.substring(4, 15)}
            />
          ))}
        </View>
      </ScrollView>
    </ImageBackground>
  );
};

OrderHistoryScreen.navigationOptions = (navData) => {
  const language = I18n.currentLocale();
  return {
    headerTitle: "ההזמנות שלי",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
    headerRight: () => {
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  bgImage: {
    width: "100%",
    height: "100%",
  },
  screen: {
    marginTop: 5,
  },
});

export default OrderHistoryScreen;
