import React, { useEffect, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  ImageBackground,
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import Colors from "../constants/Colors";
import I18n from "react-native-i18n";

const AboutUsScreen = (props) => {
  return (
    <ImageBackground
      source={require("../assets/mainBackground.png")}
      style={styles.bgImage}
    >
      <ScrollView>
        <View style={styles.screen}>
          <Text style={styles.title}>מסעדת אווז הזהב ביג באר שבע</Text>

          <View style={styles.between}></View>

          <Text style={styles.text}>
            מתוך אהבה גדולה לבשר, אנשים ושמחות, אנחנו באווז הזהב יצרנו עבורכה
            חוויה קולינארית של בשר בטעם יוצא דופן.
          </Text>
          <Text style={styles.text}>
            מעל ל-20 שנה שאנחנו מקפידים לבחור את נתחי הבשר האיכותיים ביותר,
            מוסיפים לו תבלינים סודיים, יוצרים ארומה מיוחד והתוצאה...וואו!
          </Text>
          <Text style={styles.text}>
            לצד הבשר המצוין, אנחנו דואגים לבר של סלטים טריים יום יום, סטנדרט
            שירות גבוה וצוות עובדים מקצועי, שתמיד יהיה שם בשבילכם עם חיוך.
          </Text>
          <Text style={styles.text}>
            בין אם אתם חוגגים אירוע פרטי או עסקי, או שסתם בא לכם להתפנק
            בשווארמה, יש ברשותנו כמה חללי ישיבה המותאמים לקונספט החגיגה.
          </Text>
          <Text style={styles.text}>אז... שיהיה בתאבון ותאכלו פרה פרה!</Text>

          <View style={styles.finalView}>
            <Text style={styles.finalText}>
              שני חדרי אירוח לאירועים פרטיים ועסקיים על 65 סועדים
            </Text>
            <Text style={styles.finalText}>
              ___________________________________
            </Text>
            <Text style={styles.finalText}>מתחם אקספרס למזון מהר</Text>
            <Text style={styles.finalText}>
              ___________________________________
            </Text>
            <Text style={styles.finalText}>מרפסת מקורה</Text>
            <Text style={{ marginBottom: "8%" }}></Text>
          </View>
        </View>
      </ScrollView>
    </ImageBackground>
  );
};

AboutUsScreen.navigationOptions = (navData) => {
  const language = I18n.currentLocale();
  return {
    headerTitle: "קצת עלינו",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
    headerRight: () => {
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    textAlign: "center",
    margin: "5%",
    marginTop: "3%",
    color: "white",
    fontSize: 12,
  },
  finalText: {
    textAlign: "center",
    color: Colors.accentColor,
    fontSize: 12,
  },
  finalView: {
    marginTop: "15%",
  },
  bgImage: {
    width: "80%",
    height: "80%",
  },
  title: {
    textAlign: "center",
    color: Colors.accentColor,
    fontSize: 20,
    marginBottom: "5%",
  },
  bgImage: {
    width: "100%",
    height: "100%",
  },
  between: {
    marginTop: "20%",
  },
});

export default AboutUsScreen;
