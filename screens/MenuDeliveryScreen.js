import React, { Component } from "react";
import {
  StyleSheet,
  SafeAreaView,
  ImageBackground,
  View,
  Text,
  TouchableOpacity,
} from "react-native";
import ProductsOverviewScreen from "./ProductsOverviewScreen";
import MaterialTabs from "react-native-material-tabs";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import { connect } from "react-redux";
import { NavigationEvents } from "react-navigation";
import I18n from "react-native-i18n";
import { HeaderBackButton } from "react-navigation-stack";

class MenuDeliveryScreen extends Component {
  state = {
    selectedTab: 2,
  };

  setTab(tab) {
    this.setState({ selectedTab: tab });
  }

  render() {
    const { navigation } = this.props;

    const street = navigation.getParam("street");
    const apaNumber = navigation.getParam("apaNumber");
    const entarece = navigation.getParam("entarece");
    const floor = navigation.getParam("floor");
    const apartment = navigation.getParam("apartment");
    const fullName = navigation.getParam("fullName");
    const phoneNumber = navigation.getParam("phoneNumber");
    const email = navigation.getParam("email");

    const apply = () => {
      this.props.navigation.navigate("Cart", {
        street: street,
        apaNumber: apaNumber,
        entarece: entarece,
        floor: floor,
        apartment: apartment,
        fullName: fullName,
        phoneNumber: phoneNumber,
        email: email,
      });
    };

    const language = I18n.currentLocale();

    return (
      <ImageBackground
        source={require("../assets/Background.jpg")}
        style={styles.bgImage}
      >
        <NavigationEvents
          onDidFocus={() => {
            this.setState({});
          }}
        />
        <SafeAreaView>
          <MaterialTabs
            items={[
              <Text key={1} style={styles.tabName}>
                שתייה
              </Text>,
              <Text key={2} style={styles.tabName}>
                חמגשית
              </Text>,
              <Text key={3} style={styles.tabName}>
                לאפה
              </Text>,
              <Text key={4} style={styles.tabName}>
                באגט
              </Text>,
              <Text key={5} style={styles.tabName}>
                פיתה
              </Text>,
            ]}
            selectedIndex={this.state.selectedTab}
            onChange={this.setTab.bind(this)}
            barColor="#a8883b"
          />
        </SafeAreaView>
        <View style={styles.products}>
          {this.state.selectedTab === 0 && <Text style={styles.text}></Text>}
          {this.state.selectedTab !== 0 && this.state.selectedTab !== 1 && (
            <Text style={styles.text}>* במנה כלולים ציפס וחמוצים</Text>
          )}
          {this.state.selectedTab === 1 && (
            <Text style={styles.text}>
              * במנה כלולים לחם, תוספת לבחירה ו2 סוגי סלטים
            </Text>
          )}
          <ProductsOverviewScreen selectedTab={this.state.selectedTab} />
        </View>

        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            apply();
          }}
          style={styles.touchableOpacityStyle}
        >
          <Text style={styles.textButton}>ההזמנה שלי{"    "}</Text>
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item title="Cart" iconName="ios-cart" />
          </HeaderButtons>
          <Text
            style={{
              color: "red",
              fontSize: 22,
              marginRight: "10%",
            }}
          >
            {this.props.cart.totalItems}
          </Text>
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}

MenuDeliveryScreen.navigationOptions = (navData) => {
  const language = I18n.currentLocale();
  return {
    headerTitle: "תפריט משלוחים",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language == "en-US") {
        return (
          <HeaderBackButton
            tintColor="white"
            onPress={() => {
              navData.navigation.goBack();
            }}
          />
        );
      }
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
    headerRight: () => {
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
      if (language != "en-US") {
        return (
          <HeaderBackButton
            tintColor="white"
            backImage={() => (
              <Text style={{ color: "white", fontWeight: "bold" }}>חזור</Text>
            )}
            onPress={() => {
              navData.navigation.goBack();
            }}
          />
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  touchableOpacityStyle: {
    position: "absolute",
    width: "60%",
    height: "6%",
    alignItems: "center",
    justifyContent: "space-around",
    left: "10%",
    bottom: "0.1%",
    backgroundColor: "#a8883b",
    borderRadius: 40,
    flexDirection: "row",
  },
  bgImage: {
    width: "100%",
    height: "100%",
    flex: 1,
  },
  products: {
    flex: 1,
  },
  tabName: {
    fontSize: 10,
  },
  toCartContainer: {
    height: 32,
    backgroundColor: "#a8883b",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
    borderWidth: 2,
    borderColor: "black",
  },
  textButton: {
    color: "white",
    fontSize: 18,
  },
  text: {
    color: "white",
    fontSize: 14,
  },
});

const mapStateToProps = (state) => {
  return {
    cart: state.cart,
  };
};

export default connect(mapStateToProps)(MenuDeliveryScreen);
