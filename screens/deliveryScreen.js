import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  ImageBackground,
  View,
  Text,
  TextInput,
  Alert,
  ScrollView,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import Colors from "../constants/Colors";
import DialogAlert from "../components/DialogAlert";
import DialogComponentSpinner from "../components/DialogComponentSpinner";
import { HeaderBackButton } from "react-navigation-stack";
import I18n from "react-native-i18n";

const deliveryScreen = (props) => {
  const [visible, setVisible] = useState(false);
  const [spinner, setSpinner] = useState(false);

  function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  const apply = () => {
    if (
      fullNameIsValid &&
      streetIsValid &&
      phoneNumberIsValid &&
      apaNumberIsValid &&
      entareceIsValid &&
      apartmentIsValid &&
      floorIsValid &&
      emailIsValid
    ) {
      setSpinner(true);
      setTimeout(() => {
        setSpinner(false);
        props.navigation.navigate("menuDelivery", {
          street: street,
          apaNumber: apaNumber,
          entarece: entarece,
          floor: floor,
          apartment: apartment,
          fullName: fullName,
          phoneNumber: phoneNumber,
          email: email,
        });
      }, 1800);
    } else {
      setVisible(true);
    }
  };

  const [fullName, setFullName] = useState("");
  const [street, setStreet] = useState("");
  const [apaNumber, setApaNumber] = useState(0);
  const [entarece, setEntrace] = useState(0);
  const [floor, setFloor] = useState(0);
  const [apartment, setApartment] = useState(0);
  const [phoneNumber, setPhoneNumber] = useState(0);
  const [email, setEmail] = useState("");

  const [fullNameIsValid, setFullNameIsValid] = useState(false);
  const [streetIsValid, setStreetIsValid] = useState(false);
  const [apaNumberIsValid, setApaNumberIsValid] = useState(false);
  const [entareceIsValid, setEntareceIsValid] = useState(false);
  const [floorIsValid, setFloorIsValid] = useState(false);
  const [apartmentIsValid, setApartmentIsValid] = useState(false);
  const [phoneNumberIsValid, setPhoneNumberIsValid] = useState(false);
  const [emailIsValid, setEmailIsValid] = useState(false);

  const titleChangeHandler = (text) => {
    if (text.trim().length === 0) {
      setFullNameIsValid(false);
    } else {
      setFullNameIsValid(true);
    }
    setFullName(text);
  };

  const streetChangeHandler = (text) => {
    if (text.trim().length === 0) {
      setStreetIsValid(false);
    } else {
      setStreetIsValid(true);
    }
    setStreet(text);
  };

  const apaNumberChangeHandler = (text) => {
    if (text.trim().length === 0) {
      setApaNumberIsValid(false);
    } else {
      setApaNumberIsValid(true);
    }
    setApaNumber(text);
  };

  const entareceChangeHandler = (text) => {
    if (text.trim().length === 0) {
      setEntareceIsValid(false);
    } else {
      setEntareceIsValid(true);
    }
    setEntrace(text);
  };

  const floorChangeHandler = (text) => {
    if (text.trim().length === 0) {
      setFloorIsValid(false);
    } else {
      setFloorIsValid(true);
    }
    setFloor(text);
  };

  const apartmentChangeHandler = (text) => {
    if (text.trim().length === 0) {
      setApartmentIsValid(false);
    } else {
      setApartmentIsValid(true);
    }
    setApartment(text);
  };

  const phoneNumberChangeHandler = (text) => {
    if (
      text.trim().length < 10 ||
      text[0] != 0 ||
      text[1] != 5 ||
      !Number.isInteger(parseInt(text[2])) ||
      !Number.isInteger(parseInt(text[3])) ||
      !Number.isInteger(parseInt(text[4])) ||
      !Number.isInteger(parseInt(text[5])) ||
      !Number.isInteger(parseInt(text[6])) ||
      !Number.isInteger(parseInt(text[7])) ||
      !Number.isInteger(parseInt(text[8])) ||
      !Number.isInteger(parseInt(text[9]))
    ) {
      setPhoneNumberIsValid(false);
    } else {
      setPhoneNumberIsValid(true);
    }
    setPhoneNumber(text);
  };

  const emailChangeHandler = (text) => {
    if (validateEmail(text)) {
      if (text.trim().length === 0) {
        setEmailIsValid(false);
      } else {
        setEmailIsValid(true);
      }
      setEmail(text);
    } else {
      setEmailIsValid(false);
    }
  };

  const handleCancle = () => {
    setVisible(false);
  };

  //Quit From The Currnt Page - Message To The User
  useEffect(() => {
    const backAction = () => {
      if (props.navigation.isFocused()) {
        Alert.alert(
          "חזרה לתחילת ההזמנה",
          "פעולה זו תמחק את פרטי ההזמנה הנוכחיים!",
          [
            {
              text: "ביטול",
              onPress: () => {},
              style: "cancel",
            },
            {
              text: "אישור",
              style: "destructive",
              onPress: () => props.navigation.goBack(),
            },
          ],
          { cancelable: false }
        );

        return true;
      }
    };
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }, []);

  return (
    <ImageBackground
      source={require("../assets/Background.jpg")}
      style={styles.bgImage}
    >
      <View style={styles.mainContainer}>
        <ScrollView>
          <View style={styles.inputContainer}>
            <View style={styles.row}>
              <TextInput
                style={{ ...styles.Input, marginLeft: "1%" }}
                textAlign="right"
                placeholder="רחוב"
                maxLength={23}
                placeholderTextColor="white"
                onChangeText={streetChangeHandler}
              />

              <TextInput
                style={{ ...styles.Input, marginRight: "3%" }}
                textAlign="right"
                placeholder="שם ושם משפחה"
                maxLength={20}
                placeholderTextColor="white"
                onChangeText={titleChangeHandler}
              />
            </View>

            <View style={styles.row}>
              <View style={styles.validationTextContainer}>
                {!streetIsValid && <Text style={styles.valid}>*חובה</Text>}
              </View>

              <View style={styles.validationTextContainer}>
                {!fullNameIsValid && <Text style={styles.valid}>*חובה</Text>}
              </View>
            </View>

            <View style={styles.row}>
              <TextInput
                style={{ ...styles.Input, marginLeft: "1%" }}
                textAlign="right"
                placeholder="דירה"
                maxLength={5}
                placeholderTextColor="white"
                keyboardType="decimal-pad"
                onChangeText={apartmentChangeHandler}
              />

              <TextInput
                style={{ ...styles.Input, marginRight: "3%" }}
                textAlign="right"
                placeholder="מספר"
                maxLength={5}
                keyboardType="decimal-pad"
                placeholderTextColor="white"
                onChangeText={apaNumberChangeHandler}
              />
            </View>
            <View style={styles.row}>
              <View style={styles.validationTextContainer}>
                {!apartmentIsValid && <Text style={styles.valid}>*חובה</Text>}
              </View>
              <View style={styles.validationTextContainer}>
                {!apaNumberIsValid && <Text style={styles.valid}>*חובה</Text>}
              </View>
            </View>

            <View style={styles.row}>
              <TextInput
                style={{ ...styles.Input, marginLeft: "1%" }}
                textAlign="right"
                placeholder="כניסה"
                maxLength={5}
                placeholderTextColor="white"
                keyboardType="default"
                onChangeText={entareceChangeHandler}
              />

              <TextInput
                style={{ ...styles.Input, marginRight: "3%" }}
                textAlign="right"
                placeholder="קומה"
                maxLength={5}
                placeholderTextColor="white"
                keyboardType="decimal-pad"
                onChangeText={floorChangeHandler}
              />
            </View>
            <View style={styles.row}>
              <View style={styles.validationTextContainer}>
                {!entareceIsValid && <Text style={styles.valid}>*חובה</Text>}
              </View>

              <View style={styles.validationTextContainer}>
                {!floorIsValid && <Text style={styles.valid}>*חובה</Text>}
              </View>
            </View>

            <View style={styles.row}>
              <TextInput
                style={{ ...styles.Input, marginLeft: "1%" }}
                textAlign="right"
                placeholder="טלפון"
                placeholderTextColor="white"
                keyboardType="decimal-pad"
                maxLength={10}
                onChangeText={phoneNumberChangeHandler}
              />

              <TextInput
                style={{ ...styles.Input, marginRight: "3%" }}
                textAlign="right"
                placeholder="כתובת מייל"
                placeholderTextColor="white"
                keyboardType="email-address"
                onChangeText={emailChangeHandler}
              />
            </View>

            {visible && (
              <DialogAlert
                onOk={handleCancle}
                title={"מילוי טופס"}
                message={"נא למלא את כל השדות הנדרשים"}
              />
            )}
            {spinner && <DialogComponentSpinner />}

            <View style={{ ...styles.row, marginBottom: "3%" }}>
              <View style={styles.validationTextContainer}>
                {!phoneNumberIsValid && (
                  <Text style={styles.valid}>* מספר פלאפון לוידוא במסרון</Text>
                )}
              </View>
              <View style={styles.validationTextContainer}>
                {!emailIsValid && (
                  <Text style={styles.valid}>* אימייל לשליחת הקבלה</Text>
                )}
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={styles.buttonContainerStyle}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              apply();
            }}
          >
            <Text style={styles.text}>לתפריט המשלוחים</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ImageBackground>
  );
};

deliveryScreen.navigationOptions = (navData) => {
  const language = I18n.currentLocale();

  return {
    headerTitle: "משלוח",
    headerTitleAlign: "center",
    headerLeft: () => {
      if (language != "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
      if (language == "en-US") {
        return (
          <HeaderBackButton
            tintColor="white"
            onPress={() => {
              Alert.alert(
                "חזרה לתחילת ההזמנה",
                "פעולה זו תמחק את פרטי ההזמנה הנוכחיים!",
                [
                  {
                    text: "ביטול",
                    onPress: () => {},
                    style: "cancel",
                  },
                  {
                    text: "אישור",
                    style: "destructive",
                    onPress: () => navData.navigation.goBack(),
                  },
                ],
                { cancelable: false }
              );
            }}
          />
        );
      }
    },
    headerRight: () => {
      if (language != "en-US") {
        return (
          <HeaderBackButton
            tintColor="white"
            backImage={() => (
              <Text style={{ color: "white", fontWeight: "bold" }}>חזור</Text>
            )}
            onPress={() => {
              Alert.alert(
                "חזרה לתחילת ההזמנה",
                "פעולה זו תמחק את פרטי ההזמנה הנוכחיים!",
                [
                  {
                    text: "ביטול",
                    onPress: () => {},
                    style: "cancel",
                  },
                  {
                    text: "אישור",
                    style: "destructive",
                    onPress: () => navData.navigation.goBack(),
                  },
                ],
                { cancelable: false }
              );
            }}
          />
        );
      }
      if (language == "en-US") {
        return (
          <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
              title="Menu"
              iconName="ios-menu"
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
            />
          </HeaderButtons>
        );
      }
    },
  };
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  inputContainer: {
    alignItems: "center",
    marginTop: "4%",
    borderColor: Colors.accentColor,
    borderWidth: 1,
    marginLeft: 6,
    marginRight: 6,
  },
  row: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
  },
  bgImage: {
    width: "100%",
    height: "100%",
  },
  button: {
    backgroundColor: Colors.accentColor,
    borderRadius: 10,
    width: "50%",
    height: "15%",
    justifyContent: "center",
  },
  text: {
    color: "white",
    textAlign: "center",
  },
  Input: {
    width: "47%",
    marginTop: "2%",
    borderBottomColor: Colors.accentColor,
    borderBottomWidth: 1,
    color: "white",
    padding: 6,
  },
  buttonContainerStyle: {
    justifyContent: "center",
    width: "100%",
    alignItems: "center",
    height: "48%",
  },
  valid: {
    color: "red",
    fontSize: 10,
  },
  validationTextContainer: {
    alignItems: "center",
    width: "50%",
  },
  spinnerView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
  },
});

export default deliveryScreen;
