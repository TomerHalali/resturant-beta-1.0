import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
} from "react-native";

import DefaultText from "../components/DefaultText";

const MealItemEvening = (props) => {
  return (
    <View style={styles.mealItem}>
      <View>
        <View>
          <View style={{ ...styles.mealRow, ...styles.mealHeader }}>
            <ImageBackground source={{ uri: props.image }} style={styles.bgImage}>
              <View style={styles.titleContainer}>
                <Text style={styles.title} numberOfLines={1}>
                  {props.title}
                </Text>
              </View>
            </ImageBackground>
          </View>

          <View style={{ ...styles.mealRow, ...styles.mealDetail }}>
            <DefaultText>{props.complexity.toUpperCase()}</DefaultText>
            <DefaultText>{props.affordability.toUpperCase()}</DefaultText>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mealRow: {
    flexDirection: "row",
  },
  mealItem: {
    height: 150,
    width: "50%",
    backgroundColor: "black",
    borderRadius: 20,
    overflow: "hidden",
    marginVertical: 10,
    marginTop: "2%",
    flex: 1,
    marginRight: "2%",
    marginLeft: "2%"
  },
  bgImage: {
    width: "100%",
    height: "100%",
    justifyContent: "flex-end",
  },
  mealHeader: {
    height: "85%",
  },
  mealDetail: {
    paddingHorizontal: 10,
    justifyContent: "space-between",
    alignItems: "center",
    height: "15%",
  },
  titleContainer: {
    backgroundColor: "rgba(0,0,0,0.5)",
    paddingVertical: 2,
    paddingHorizontal: 12,
  },
  title: {
    fontSize: 10,
    color: "white",
    textAlign: "center",
  },
});

export default MealItemEvening;

