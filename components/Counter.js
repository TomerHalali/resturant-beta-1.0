import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

const Counter = (props) => {
  
  return (
    <View style={styles.container}>
      
      <TouchableOpacity
        onPress={() => {
          props.minus();
        }}
      >
        {props.quantity>1 && <Text style={styles.textBTN}>-</Text>}
      </TouchableOpacity>

      <Text style={styles.text}>{props.quantity}</Text>

      <TouchableOpacity
        onPress={() => {
          props.plus();
        }}
      >
        {props.quantity<20 && <Text style={styles.textBTN}>+</Text>}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 50,
  },
  text: {
    color: "white",
    fontSize: 17,
  },
  textBTN: {
    color: "white",
    fontSize: 22,
  },
});

export default Counter;
