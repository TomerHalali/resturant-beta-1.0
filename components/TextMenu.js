import React from "react";
import { StyleSheet, Text, View } from "react-native";
import I18n from "react-native-i18n";

const TextMenu = (props) => {
  return (
    <View style={styles.screen}>
      <Text style={styles.ourText}>
        <Text>{props.title}</Text>
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  ourText: {
    textAlign: I18n.currentLocale() == "en-US" ? "right" : "left",
    width: "90%",
    paddingVertical: "4%",
    paddingHorizontal: "3%",
  },
  screen: {
    flex: 1,
    paddingTop: "4%",
  },
});

export default TextMenu;
