import React, { useState } from "react";
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import Dialog from "react-native-dialog";

const DialogComponentPlate = (props) => {
  const lafa = props.lafa;
  const pita = props.pita;
  const baget = props.baget;
  const rice = props.rice;
  const fries = props.fries;
  const puree = props.puree;
  const potato = props.potato;
  const pickles = props.pickles;
  const carrot = props.carrot;
  const shivka = props.shivka;
  const krovW = props.krovW;

  const [spinner, setSpinner] = useState(false);

  return (
    <Dialog.Container contentStyle={styles.dialogContainer} visible={true}>
      {!spinner && (
        <View
          style={{
            justifyContent: "space-around",
            flexDirection: "row",
            marginBottom: 4,
          }}
        >
          <Text style={styles.mainTitle}> כמות: {props.QUT}</Text>
          <Text style={styles.mainTitle}>{props.title}</Text>
        </View>
      )}
      {!spinner && <Text style={styles.subTitle}>בחר סוג לחם</Text>}
      {!spinner && (
        <View style={styles.container}>
          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: pita ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.pitaClick();
            }}
          >
            <Text style={styles.textBeforClick}>פיתה</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: lafa ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.lafaClick();
            }}
          >
            <Text style={styles.textBeforClick}>לאפה</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: baget ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.bagetClick();
            }}
          >
            <Text style={styles.textBeforClick}>בגט</Text>
          </TouchableOpacity>
        </View>
      )}
      {!spinner && <Text style={styles.subTitle}>בחר תוספת</Text>}

      {!spinner && (
        <View style={styles.container}>
          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: potato ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.potatoClick();
            }}
          >
            <Text style={styles.textBeforClick}>תפו"א</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: puree ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.pureeClick();
            }}
          >
            <Text style={styles.textBeforClick}>פירה</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: fries ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.friesClick();
            }}
          >
            <Text style={styles.textBeforClick}>ציפס</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: rice ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.riceClick();
            }}
          >
            <Text style={styles.textBeforClick}>אורז</Text>
          </TouchableOpacity>
        </View>
      )}
      {!spinner && <Text style={styles.subTitle}>בחר 2 סלטים</Text>}

      {!spinner && (
        <View style={styles.container}>
          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: shivka ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.shivkaClick();
            }}
          >
            <Text style={styles.textBeforClick}>שבקה</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: krovW ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.krovWClick();
            }}
          >
            <Text style={styles.textBeforClick}>כרוב לבן</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: carrot ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.carrotClick();
            }}
          >
            <Text style={styles.textBeforClick}>גזר מגורד</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: pickles ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.picklesClick();
            }}
          >
            <Text style={styles.textBeforClick}>חמוצים</Text>
          </TouchableOpacity>
        </View>
      )}

      {!spinner && (
        <View style={styles.buttonsContainer}>
          <Dialog.Button
            style={styles.button}
            label="בטל"
            onPress={props.onCancle}
          />
          <Dialog.Button
            style={styles.button}
            label="הוסף מנה"
            onPress={function ok() {
              setSpinner(true);
              setTimeout(() => {
                props.onOk();
              }, 1200);
            }}
          />
        </View>
      )}
      {spinner && <ActivityIndicator size="large" color="#a8883b" />}
    </Dialog.Container>
  );
};

const styles = StyleSheet.create({
  dialogContainer: {
    borderColor: "#a8883b",
    borderWidth: 3,
    borderRadius: 15,
  },
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
  },
  title: {
    textAlign: "center",
    fontSize: 18,
    color: "#a8883b",
    fontWeight: "bold",
  },
  mainTitle: {
    textAlign: "right",
    fontSize: 14,
    color: "#a8883b",
    fontWeight: "bold",
  },
  textBeforClick: {
    textAlign: "right",
  },
  TouchableContainer: {
    borderColor: "#888",
    borderWidth: 2,
    padding: 4,
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  button: {
    color: "#a8883b",
    fontWeight: "bold",
    marginTop: 10,
  },
  subTitle: {
    textAlign: "center",
    fontSize: 18,
    color: "#a8883b",
    fontWeight: "bold",
  },
});

export default DialogComponentPlate;
