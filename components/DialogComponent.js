import React, { useState } from "react";
import { Text, StyleSheet, View, TouchableOpacity, ActivityIndicator } from "react-native";
import Dialog from "react-native-dialog";

const DialogComponent = (props) => {
  const humus = props.humus;
  const onion = props.onion;
  const arisa = props.arisa;
  const lemon = props.lemon;
  const krovW = props.krovW;
  const krovP = props.krovP;
  const hazil = props.hazil;
  const petro = props.petro;
  const salad = props.salad;
  const tahini = props.tahini;
  const amba = props.amba;

  const [spinner, setSpinner] = useState(false);

  return (
    <Dialog.Container contentStyle={styles.dialogContainer} visible={true}>
      {!spinner && (
        <View style={{ justifyContent: "space-around", flexDirection: "row" }}>
          <Text style={styles.mainTitle}> כמות: {props.QUT}</Text>
          <Text style={styles.mainTitle}>{props.title}</Text>
        </View>
      )}
      {!spinner && (
        <Dialog.Title style={styles.title}>מה לשים בפנים ?</Dialog.Title>
      )}
      {!spinner && (
        <View style={styles.container}>
          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: onion ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.onionClick();
            }}
          >
            <Text style={styles.textBeforClick}>בצל</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: lemon ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.lemonClick();
            }}
          >
            <Text style={styles.textBeforClick}>לימון כבוש</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: arisa ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.arisaClick();
            }}
          >
            <Text style={styles.textBeforClick}>חריף</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: humus ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.humusClick();
            }}
          >
            <Text style={styles.textBeforClick}>חומוס</Text>
          </TouchableOpacity>
        </View>
      )}
      {!spinner && (
        <View style={styles.container}>
          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: krovW ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.krovWClick();
            }}
          >
            <Text style={styles.textBeforClick}>כרוב לבן</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: krovP ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.krovPClick();
            }}
          >
            <Text style={styles.textBeforClick}>כרוב סגול</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: salad ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.saladClick();
            }}
          >
            <Text style={styles.textBeforClick}>סלט ירקות</Text>
          </TouchableOpacity>
        </View>
      )}
      {!spinner && (
        <View style={styles.container}>
          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: hazil ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.hazilClick();
            }}
          >
            <Text style={styles.textBeforClick}>חצילים</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: petro ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.petroClick();
            }}
          >
            <Text style={styles.textBeforClick}>פטרוזיליה</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: tahini ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.tahiniClick();
            }}
          >
            <Text style={styles.textBeforClick}>טחינה</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              ...styles.TouchableContainer,
              backgroundColor: amba ? "#9AF5A6" : "white",
            }}
            onPress={() => {
              props.ambaClick();
            }}
          >
            <Text style={styles.textBeforClick}>עמבה</Text>
          </TouchableOpacity>
        </View>
      )}
      {!spinner && (
        <View style={styles.buttonsContainer}>
          <Dialog.Button
            style={styles.button}
            label="בטל"
            onPress={props.onCancle}
          />
          <Dialog.Button
            style={styles.button}
            label="הוסף מנה"
            onPress={function ok() {
              setSpinner(true);
              setTimeout(() => {
                props.onOk();
              }, 1200);
            }}
          />
        </View>
      )}
      {spinner && <ActivityIndicator size="large" color="#a8883b" />}
    </Dialog.Container>
  );
};

const styles = StyleSheet.create({
  dialogContainer: {
    borderColor: "#a8883b",
    borderWidth: 3,
    borderRadius: 15,
  },
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 15,
  },
  title: {
    textAlign: "center",
    fontSize: 18,
    color: "#a8883b",
    fontWeight: "bold",
  },
  mainTitle: {
    textAlign: "right",
    fontSize: 14,
    color: "#a8883b",
    fontWeight: "bold",
  },
  textBeforClick: {
    textAlign: "right",
  },
  TouchableContainer: {
    borderColor: "#888",
    borderWidth: 2,
    padding: 4,
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  button: {
    color: "#a8883b",
    fontWeight: "bold",
    marginTop: 10,
  },
});

export default DialogComponent;
