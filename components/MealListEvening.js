import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
import MealItemEvening from "../components/MealItemEvening";



const MealListEvening = (props) => {
  
  const renderMealItem = (itemData) => {

    return (
      <MealItemEvening
        title={itemData.item.title}
        image={itemData.item.imageUrl}
        duration={itemData.item.duration}
        complexity={itemData.item.complexity}
        affordability={itemData.item.affordability}
      />
    );
  };

  return (
    <View style={styles.list}>
      <FlatList
        data={props.listData}
        keyExtractor={(item, index) => item.id}
        renderItem={renderMealItem}
        style={{ width: "100%" }}
        numColumns={2}
        key={(item, index) => item.id}
        bounces={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  list: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "black",
  },
});

export default MealListEvening;
