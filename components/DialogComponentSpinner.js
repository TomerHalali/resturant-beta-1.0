import React from "react";
import { View, StyleSheet, ActivityIndicator } from "react-native";
import Dialog from "react-native-dialog";

const DialogComponentSpinner = (props) => {

  return (
    <Dialog.Container contentStyle={styles.dialogContainer} visible={true}>
      <View style={styles.spinnerView}>
        <ActivityIndicator size="large" color="#a8883b" />
      </View>
    </Dialog.Container>
  );
};


const styles = StyleSheet.create({
  dialogContainer: {
    borderColor: "#a8883b",
    borderWidth: 1,
    borderRadius: 15,
    height: "36%",
    backgroundColor: '#202020',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  spinnerView: {
    alignItems: "center",
    justifyContent: "center",
    width: "30%",
    height: "30%"
  },
});

export default DialogComponentSpinner;
