import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import CartItem from "./shop/CartItem";

const HistoryBlock = (props) => {
  const [showDetails, setShowDetails] = useState(false);

  return (
    <View style={styles.container}>
      <View style={styles.itemDetails}>
        <Text style={{color: "#a8883b"}}
          onPress={() => {
            setShowDetails((prevState) => !prevState);
          }}
        >
          {showDetails ? "הסתר פרטים" : "הצגת פרטים"}
        </Text>
        <Text>סכום: ₪{props.sum}</Text>
        <Text>תאריך: {props.date}</Text>
      </View>
      <View>
        {showDetails && (
          <View style={styles.detailItems}>
            {props.items.map((cartItem) => (
              <CartItem
                key={cartItem.productId}
                quantity={cartItem.quantity}
                amount={cartItem.sum}
                title={cartItem.productTitle}
                whatInside={cartItem.whatInside}
              />
            ))}
          </View>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  itemDetails: {
    padding: 10,
    backgroundColor: "white",
    marginHorizontal: 22,
    marginVertical: 8,
    borderRadius: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    borderWidth: 3,
    borderColor: '#a8883b'
  },
});

export default HistoryBlock;
