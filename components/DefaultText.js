import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const DefaultText = props => {
    return <Text style={{ ...styles.text, ...styles.text}} >{props.children}</Text>
    
};

const styles = StyleSheet.create({
    text:{
        color: 'white',
        fontSize: 10
     
    }
});

export default DefaultText;
