import React from "react";
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableNativeFeedback,
  ImageBackground,
} from "react-native";

import Colors from "../constants/Colors";

const CategoryGridTile = (props) => {
  let TouchableCmp = TouchableOpacity;

  if (Platform.OS === "android" && Platform.Version >= 21) {
    TouchableCmp = TouchableNativeFeedback;
  }

  return (
    <View style={styles.screen}>
      <View style={styles.gridItem}>
        <TouchableCmp style={{ flex: 1 }} onPress={props.onSelect}>
          <View style={{ ...styles.container, ...{} }}>
            <ImageBackground
              source={{ uri: props.image }}
              style={styles.bgImage}
            />
            <Text style={styles.title}>{props.title}</Text>
          </View>
        </TouchableCmp>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "black",
  },
  gridItem: {
    flex: 1,
    margin: 15,
    height: 150,
    borderRadius: 5,
    overflow:
      Platform.OS === "android" && Platform.Version >= 21
        ? "hidden"
        : "visible",
    elevation: 4,
  },
  container: {
    flex: 1,
    borderRadius: 2,
    shadowColor: "black",
    elevation: 3,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  title: {
    fontSize: 12,
    color: Colors.accentColor,
  },
  bgImage: {
    width: "100%",
    height: "100%",
  },
});

export default CategoryGridTile;

