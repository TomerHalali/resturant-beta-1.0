import React, { useState } from "react";
import { Text, StyleSheet, View, ActivityIndicator } from "react-native";
import Dialog from "react-native-dialog";

const DialogComponent = (props) => {
  const [spinner, setSpinner] = useState(false);

  return (
    <Dialog.Container contentStyle={styles.dialogContainer} visible={true}>
      {!spinner && (
        <View>
          <Text style={styles.title}>{props.title}</Text>
        </View>
      )}
      {!spinner && (
        <View>
          <Text style={{ ...styles.title, fontSize: 14, color: "black" }}>
            כמות: {props.QUT}
          </Text>
        </View>
      )}
      {!spinner && (
        <View style={styles.buttonsContainer}>
          <Dialog.Button
            style={styles.button}
            label="בטל"
            onPress={props.onCancle}
          />
          <Dialog.Button
            style={styles.button}
            label="הוסף להזמנה"
            onPress={function ok() {
              setSpinner(true);
              setTimeout(() => {
                props.onOk();
              }, 1200);
            }}
          />
        </View>
      )}
      {spinner && <ActivityIndicator size="large" color="#a8883b" />}
    </Dialog.Container>
  );
};

const styles = StyleSheet.create({
  dialogContainer: {
    borderColor: "#a8883b",
    borderWidth: 3,
    borderRadius: 15,
  },
  buttonsContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  button: {
    color: "#a8883b",
    fontWeight: "bold",
    marginTop: 10,
  },
  title: {
    textAlign: "center",
    fontSize: 20,
    color: "#a8883b",
    fontWeight: "bold",
  },
});

export default DialogComponent;
