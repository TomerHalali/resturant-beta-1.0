import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
import MealItem from "../components/MealItem";

const MealList = (props) => {

  const renderMealItem = (itemData) => {

    return (
      <MealItem
        title={itemData.item.title}
        image={itemData.item.imageUrl}
        duration={itemData.item.duration}
        complexity={itemData.item.complexity}
        affordability={itemData.item.affordability}
        onSelectMeal={() => {
          props.navigation.navigate({
            routeName: "MealDetail",
            params: {
              item: itemData.item
            },
          });
        }}
      />
    );
  };

  return (
    <View style={styles.list}>
      <FlatList
        data={props.listData}
        keyExtractor={(item, index) => item.title}
        renderItem={renderMealItem}
        style={{ width: "100%" }}
        bounces={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  list: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "black",
  },
});

export default MealList;
