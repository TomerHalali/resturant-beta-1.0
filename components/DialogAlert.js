import React from "react";
import { Text, StyleSheet, View } from "react-native";
import Dialog from "react-native-dialog";

const DialogComponent = (props) => {
  return (
    <Dialog.Container contentStyle={styles.dialogContainer} visible={true}>
      <Dialog.Title style={styles.title}>{props.title}</Dialog.Title>
      <Text>{props.message}</Text>

      <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
        {props.screen == "Main" && (
          <Dialog.Button
            style={styles.button}
            label="ברצוני להישאר"
            onPress={props.onCancle}
          />
        )}
        <Dialog.Button
          style={styles.button}
          label={props.screen == "Main" ? "כן, לצאת" : "חזור"}
          onPress={props.onOk}
        />
      </View>
    </Dialog.Container>
  );
};

const styles = StyleSheet.create({
  dialogContainer: {
    borderColor: "#a8883b",
    borderWidth: 3,
    borderRadius: 15,
  },
  buttonsContainer: {
    alignItems: "center",
  },
  button: {
    color: "#a8883b",
    fontWeight: "bold",
    marginTop: 10,
  },
  title: {
    textAlign: "center",
    fontSize: 16,
    color: "#a8883b",
    fontWeight: "bold",
  },
});

export default DialogComponent;
