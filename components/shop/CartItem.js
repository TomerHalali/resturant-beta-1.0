import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Feather } from "@expo/vector-icons";
import I18n from "react-native-i18n";

const CartItem = (props) => {
  const language = I18n.currentLocale();
  return (
    <View style={styles.cartItem}>
      <View>
        <View style={styles.itemData}>
          {language != "en-US" ? (
            <Text style={styles.quantity}>כמות: {props.quantity} </Text>
          ) : (
            props.daletable && (
              <TouchableOpacity
                onPress={props.onRemove}
                style={styles.deleteButton}
              >
                <Text style={{ color: "red", fontSize: 20 }}>
                  <Feather name="minus" size={32} />
                </Text>
              </TouchableOpacity>
            )
          )}
          {language != "en-US" ? (
            <Text style={styles.mainText}> {props.title}</Text>
          ) : (
            <Text style={styles.mainText}> ₪{props.amount.toFixed(2)}</Text>
          )}
          {language != "en-US" ? (
            <Text style={styles.mainText}> ₪{props.amount.toFixed(2)}</Text>
          ) : (
            <Text style={styles.mainText}> {props.title}</Text>
          )}

          {language != "en-US" ? (
            props.daletable && (
              <TouchableOpacity
                onPress={props.onRemove}
                style={styles.deleteButton}
              >
                <Text style={{ color: "red", fontSize: 20 }}>
                  <Feather name="minus" size={32} />
                </Text>
              </TouchableOpacity>
            )
          ) : (
            <Text style={styles.quantity}>כמות: {props.quantity} </Text>
          )}
        </View>
        <View>
          <Text style={styles.inside}>{props.whatInside}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cartItem: {
    padding: 10,
    backgroundColor: "white",
    marginHorizontal: 20,
    marginVertical: 8,
    borderRadius: 20,
  },
  itemData: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  mainText: {
    color: "#888",
    fontSize: 12,
  },
  deleteButton: {
    marginLeft: 6,
  },
  inside: {
    color: "#888",
    fontSize: 10,
  },
  quantity: {
    color: "#a8883b",
  },
});

export default CartItem;
