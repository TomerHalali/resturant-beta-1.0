import React, { useState } from "react";
import { StyleSheet, Text, View, Button, TouchableOpacity } from "react-native";
import CartItem from "./CartItem";

const OrderItem = (props) => {
  const [showDetails, setShowDetails] = useState(true);

  return (
    <View style={styles.orderItem}>
      <View style={styles.summary}>
        <Text style={styles.totalAmount}>טלפון: {props.phoneNumber}</Text>

        <Text style={styles.totalAmount}>שם: {props.fullName}</Text>
      </View>
      <View style={{alignItems: "center"}}>
        <Text style={styles.totalAmount}>רחוב: {props.street}</Text>
      </View>
      <View style={styles.summary}>
        <Text style={styles.totalAmount}>
          {" "}
          סה"כ: ₪{props.amount.toFixed(2)}
        </Text>
        <Text style={styles.date}>תאריך: {props.date}</Text>
      </View>

      <View style={styles.buttons}>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.button}
            onPress={() => {
              setShowDetails((prevState) => !prevState);
            }}
          >
            <Text style={styles.textOnButtons}>
              {showDetails ? "הסתר פרטי הזמנה" : "הצג פרטי הזמנה"}
            </Text>
          </TouchableOpacity>
        </View>
      </View>

      {showDetails && (
        <View style={styles.detailItems}>
          {props.items.map((cartItem) => (
            <CartItem
              key={cartItem.productId}
              quantity={cartItem.quantity}
              amount={cartItem.sum}
              title={cartItem.productTitle}
              whatInside={cartItem.whatInside}
            />
          ))}
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  orderItem: {
    borderRadius: 25,
    backgroundColor: "#202020",
    margin: 20,
    padding: 10,
  },
  summary: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    marginBottom: 15,
  },
  totalAmount: {
    fontSize: 12,
    color: "white",
  },
  date: {
    fontSize: 12,
    color: "white",
  },
  detailItems: {
    width: "100%",
    marginTop: "5%",
  },
  buttons: {
    marginHorizontal: "20%",
  },
  textOnButtons: {
    fontSize: 14,
    color: "white",
  },
  buttonContainer: {
    height: 30,
  },
  button: {
    backgroundColor: "#a8883b",
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 25,
  },
});

export default OrderItem;
