import React, { useState } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import Colors from "../../constants/Colors";
import Counter from "../Counter";
import DialogComponent from "../DialogComponent";
import DialogComponentPlate from "../DialogComponentPlate";
import DialogComponentDrinks from "../DialogComponentDrinks";
import I18n from "react-native-i18n";

const ProductItem = (props) => {
  const [QUT, setQUT] = useState(1);

  const plus = () => {
    if (QUT < 20) {
      setQUT(QUT + 1);
    }
  };

  const minus = () => {
    if (QUT !== 1) {
      setQUT(QUT - 1);
    }
  };
  const handleCancle = () => {
    setVisible(false);
    setVisiblePlate(false);
    setVisibleDrinks(false);
  };

  const handleOk = () => {
    setVisible(false);
    setVisiblePlate(false);
    setVisibleDrinks(false);
    props.onAddToCart(QUT, whatToPutInsideArr);
    setQUT(1);
    setHumus(false);
    setOnion(false);
    setArisa(false);
    setLemon(false);
    setKrovW(false);
    setKrovP(false);
    setHazil(false);
    setPetro(false);
    setSalad(false);
    setTahini(false);
    setAmba(false);
    setLafa(false);
    setPita(false);
    setBaget(false);
    setRice(false);
    setFries(false);
    setPuree(false);
    setPotato(false);
    setPickles(false);
    setCarrot(false);
    setShivka(false);
  };

  const [visible, setVisible] = useState(false);
  const [visiblePlate, setVisiblePlate] = useState(false);
  const [visibleDrinks, setVisibleDrinks] = useState(false);

  const [humus, setHumus] = useState(false);
  const [onion, setOnion] = useState(false);
  const [arisa, setArisa] = useState(false);
  const [lemon, setLemon] = useState(false);
  const [krovW, setKrovW] = useState(false);
  const [krovP, setKrovP] = useState(false);
  const [hazil, setHazil] = useState(false);
  const [petro, setPetro] = useState(false);
  const [salad, setSalad] = useState(false);
  const [tahini, setTahini] = useState(false);
  const [amba, setAmba] = useState(false);

  const handleSetHumus = () => {
    setHumus((prev) => !prev);
  };
  const handleSetOnion = () => {
    setOnion((prev) => !prev);
  };
  const handleSetArisa = () => {
    setArisa((prev) => !prev);
  };
  const handleSetLemon = () => {
    setLemon((prev) => !prev);
  };
  const handleSetKrovW = () => {
    if ((carrot && shivka) || (shivka && pickles) || (carrot && pickles)) {
    } else {
      setKrovW((prev) => !prev);
    }
  };
  const handleSetKrovP = () => {
    setKrovP((prev) => !prev);
  };
  const handleSetHazil = () => {
    setHazil((prev) => !prev);
  };
  const handleSetPetro = () => {
    setPetro((prev) => !prev);
  };
  const handleSetSalasd = () => {
    setSalad((prev) => !prev);
  };
  const handleSetTahini = () => {
    setTahini((prev) => !prev);
  };
  const handleSetAmba = () => {
    setAmba((prev) => !prev);
  };

  // plate:
  const [lafa, setLafa] = useState(false);
  const [pita, setPita] = useState(false);
  const [baget, setBaget] = useState(false);
  const [rice, setRice] = useState(false);
  const [fries, setFries] = useState(false);
  const [puree, setPuree] = useState(false);
  const [potato, setPotato] = useState(false);
  const [pickles, setPickles] = useState(false);
  const [carrot, setCarrot] = useState(false);
  const [shivka, setShivka] = useState(false);

  const handleSetLafa = () => {
    setPita(false);
    setBaget(false);
    setLafa((prev) => !prev);
  };
  const handleSetPita = () => {
    setLafa(false);
    setBaget(false);
    setPita((prev) => !prev);
  };
  const handleSetBaget = () => {
    setPita(false);
    setLafa(false);
    setBaget((prev) => !prev);
  };
  const handleSetRice = () => {
    setRice((prev) => !prev);
    setFries(false);
    setPuree(false);
    setPotato(false);
  };
  const handleSetFries = () => {
    setFries((prev) => !prev);
    setRice(false);
    setPuree(false);
    setPotato(false);
  };
  const handleSetPuree = () => {
    setPuree((prev) => !prev);
    setFries(false);
    setRice(false);
    setPotato(false);
  };
  const handleSetPotato = () => {
    setPotato((prev) => !prev);
    setFries(false);
    setPuree(false);
    setRice(false);
  };
  const handleSetPickels = () => {
    if ((krovW && shivka) || (carrot && shivka) || (krovW && carrot)) {
    } else {
      setPickles((prev) => !prev);
    }
  };
  const handleSetCarrot = () => {
    if ((krovW && shivka) || (pickles && shivka) || (krovW && pickles)) {
    } else {
      setCarrot((prev) => !prev);
    }
  };
  const handleSetshivka = () => {
    if ((krovW && carrot) || (carrot && pickles) || (krovW && pickles)) {
    } else {
      setShivka((prev) => !prev);
    }
  };

  const whatToPutInsideArr = {
    חומוס: humus,
    בצל: onion,
    אריסה: arisa,
    לימון: lemon,
    "כרוב לבן": krovW,
    "כרוב סגול": krovP,
    חציל: hazil,
    פטרוזיליה: petro,
    "סלט ירקות": salad,
    טחינה: tahini,
    עמבה: amba,
    פיתה: pita,
    לאפה: lafa,
    באגט: baget,
    אורז: rice,
    ציפס: fries,
    פירה: puree,
    "תפו'א": potato,
    חמוצים: pickles,
    "גזר מגורד": carrot,
    שבקה: shivka,
  };

  // // OS Langauge == English
  // if (I18n.currentLocale() != "en-US") {
  //   return <View></View>;
  // }

  // OS Langauge == English
  return (
    <View style={styles.product}>
      <View style={styles.details}>
        {I18n.currentLocale() != "en-US" ? (
          <View style={styles.imageContainer}>
            <Image style={styles.image} source={{ uri: props.image }} />
          </View>
        ) : (
          <Counter quantity={QUT} plus={plus} minus={minus} /> //1
        )}
        {I18n.currentLocale() != "en-US" ? (
          <Text style={styles.title}>{props.title}</Text>
        ) : (
          <Text style={styles.price}>₪{props.price.toFixed(2)}</Text> //2
        )}
        {I18n.currentLocale() != "en-US" ? (
          <Text style={styles.price}>₪{props.price.toFixed(2)}</Text>
        ) : (
          <Text style={styles.title}>{props.title}</Text> //3
        )}
        {I18n.currentLocale() != "en-US" ? (
          <Counter quantity={QUT} plus={plus} minus={minus} />
        ) : (
          //4
          <View style={styles.imageContainer}>
            <Image style={styles.image} source={{ uri: props.image }} />
          </View>
        )}
      </View>

      <View style={styles.btnContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            if (props.selectedTab === 1) {
              setVisiblePlate(true);
            } else if (props.selectedTab === 0) {
              setVisibleDrinks(true);
            } else if (props.selectedTab !== 0) {
              setVisible(true);
            }
          }}
        >
          <Text style={styles.btntext}>הוסף להזמנה</Text>
        </TouchableOpacity>
        {visiblePlate && (
          <DialogComponentPlate
            humus={humus}
            onion={onion}
            arisa={arisa}
            lemon={lemon}
            krovW={krovW}
            krovP={krovP}
            hazil={hazil}
            petro={petro}
            salad={salad}
            tahini={tahini}
            amba={amba}
            pita={pita}
            lafa={lafa}
            baget={baget}
            rice={rice}
            fries={fries}
            puree={puree}
            potato={potato}
            pickles={pickles}
            carrot={carrot}
            shivka={shivka}
            onOk={handleOk}
            onCancle={handleCancle}
            onionClick={handleSetOnion}
            humusClick={handleSetHumus}
            lemonClick={handleSetLemon}
            arisaClick={handleSetArisa}
            krovWClick={handleSetKrovW}
            krovPClick={handleSetKrovP}
            ambaClick={handleSetAmba}
            tahiniClick={handleSetTahini}
            saladClick={handleSetSalasd}
            petroClick={handleSetPetro}
            hazilClick={handleSetHazil}
            pitaClick={handleSetPita}
            bagetClick={handleSetBaget}
            lafaClick={handleSetLafa}
            riceClick={handleSetRice}
            friesClick={handleSetFries}
            pureeClick={handleSetPuree}
            potatoClick={handleSetPotato}
            picklesClick={handleSetPickels}
            carrotClick={handleSetCarrot}
            shivkaClick={handleSetshivka}
            title={props.title}
            QUT={QUT}
          />
        )}

        {visibleDrinks && (
          <DialogComponentDrinks
            onOk={handleOk}
            onCancle={handleCancle}
            title={props.title}
            QUT={QUT}
          />
        )}

        {visible && (
          <DialogComponent
            humus={humus}
            onion={onion}
            arisa={arisa}
            lemon={lemon}
            krovW={krovW}
            krovP={krovP}
            hazil={hazil}
            petro={petro}
            salad={salad}
            tahini={tahini}
            amba={amba}
            onOk={handleOk}
            onCancle={handleCancle}
            onionClick={handleSetOnion}
            humusClick={handleSetHumus}
            lemonClick={handleSetLemon}
            arisaClick={handleSetArisa}
            krovWClick={handleSetKrovW}
            krovPClick={handleSetKrovP}
            ambaClick={handleSetAmba}
            tahiniClick={handleSetTahini}
            saladClick={handleSetSalasd}
            petroClick={handleSetPetro}
            hazilClick={handleSetHazil}
            title={props.title}
            QUT={QUT}
          />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  checkBoxes: {
    borderColor: "blue",
    borderWidth: 3,
  },
  product: {
    borderRadius: 25,
    backgroundColor: "black",
    height: 85,
    marginHorizontal: "2%",
    width: "95%",
    alignItems: "center",
    marginTop: "4%",
    marginBottom: "2%",
    padding: 2,
  },
  details: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    height: 60,
    padding: 5,
  },
  title: {
    fontSize: 16,
    color: "white",
  },
  price: {
    fontSize: 14,
    color: "white",
  },
  imageContainer: {
    width: "15%",
    height: "90%",
    marginRight: 4,
    marginTop: 12,
  },
  image: {
    width: "100%",
    height: "100%",
    borderRadius: 20,
  },
  btnContainer: {
    width: "36%",
  },
  button: {
    borderRadius: 10,
    width: "100%",
    height: "46%",
    justifyContent: "center",
  },
  btntext: {
    color: Colors.accentColor,
    textAlign: "center",
    fontSize: 15,
    borderWidth: 0.6,
    borderColor: Colors.accentColor,
    borderRadius: 10,
  },
});

export default ProductItem;
